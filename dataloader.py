import math
import torch



class Dataloader:
    """A Dataloader class for numpy data
    The data is shuffled randomly after each complete pass through the Loader.
    The last batch is always included, even if there is not enough data to fill
    it completely

    Arguments:
    - data: A numpy array of data to be made available by the Loader
    - labels: A numpy array of labels corresponding to the datapoints in data.
              Must have the same length as data. If no labels are available,
              use a constant array, the same length as data.
    - batchsize: The size of a batch of data to be returned at a time."""

    def __init__(self, data, labels, batchsize):
        assert len(data) == len(labels), "Require the same amount of labels as data"
        self.data = data
        self.labels = labels
        self.bs = batchsize

        self.permute() # Create an initial permutation
        self.current = 0 # Index of the next batch

    def permute(self):
        # Shuffles the order of the internal data and label array
        perm = torch.randperm(len(self.data), device=self.data.device)
        self.data_perm = self.data[perm]
        self.labels_perm = self.labels[perm]

    def __len__(self):
        return math.ceil(len(self.data)/self.bs)

    def __next__(self):
        if self.current < len(self):
            lower, upper = self.current*self.bs, (self.current+1)*self.bs
            batch = self.data_perm[lower:upper], self.labels_perm[lower:upper]
            self.current += 1
            return batch
        else:
            self.permute()
            raise StopIteration

    def __get__(self, ind):
        return self.data[i], self.labels[i]

    def __iter__(self):
        self.current = 0
        self.permute()
        return self
