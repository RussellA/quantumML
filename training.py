from losses import CostMSE, CostGaussian, CostFidelity, CostVAE, CostKL, CostIdentity
from util import  pad, rescale, InvariantMassZ, TransverseMomentumZ
from tqdm import tqdm, trange
from optimizer import Adam, GradientDescent
from torch.optim.lr_scheduler import ExponentialLR, OneCycleLR
import torch
from vqc import pin_x, circuit
import pennylane as qml
import time
from math import ceil
import copy

torch.pi = qml.numpy.pi
#-------------------------------------------------------------------------------
# Model validation

def validate(valloader, model):
    """Compute the validation loss for a classifier"""
    val_loss = 0
    for x, y in valloader:
        batch_loss = cost_fncs[0](model.var(), model, x, y)
        val_loss += batch_loss/len(valloader)
    return val_loss

def qinn_validate(valloader, model, cost_fncs, only_MMD=False):
    """Compute the validation metrics for a QINN"""
    fidelity = 0
    gauss_l = 0
    fid_l = 0
    other_l = 0
    for x, y in valloader:
        i = 0 # index for current loss function
        if not only_MMD:
            # gaussian validation loss
            gauss_l += cost_fncs[i](model, x, y, validation=True)
            # fidelity loss with validation = True -> (fidelity_loss, fidelity)
            fid_ret = cost_fncs[i+1](model, x, y, validation=True)
            # fidelity validation loss
            fid_l += fid_ret[0]
            fidelity += fid_ret[1]
            i += 2
        for cost_fnc in cost_fncs[i:]:
            other_l += cost_fnc(model, x, y, validation=True)

    return gauss_l/len(valloader), fid_l/len(valloader), fidelity/len(valloader), other_l/len(valloader)

def inn_validate(valloader, model, cost_fncs):
    """Compute the validation metrics for a QINN"""
    gauss_l = 0
    other_l = 0
    for x, y in valloader:
        x, y = x.float(), y.float()
        batch_l = cost_fncs[0](model, x, y, validation=True)
        gauss_l += batch_l
        for cost_fnc in cost_fncs[1:]:
            batch_l = cost_fnc(model, x, y, validation=True)
            other_l += batch_l
    return gauss_l/len(valloader), other_l/len(valloader)


def accuracy(valloader, model):
    """Compute the validation accuracy for a classifier"""
    acc = 0.0
    for x, y in valloader:
        out = torch.Tensor(model[x]).squeeze()
        # Decision boundary at 0, i.e. sign == label
        acc += torch.sum(torch.sign(y) == torch.sign(out))/(len(valloader)*x.shape[0])
    return acc

#-------------------------------------------------------------------------------
# Optimizer and scheduler

def get_optimizer(params, model):
    """Creates an optimizer based on the settings in params. If lr and weight
    decay are lists, they each group is given an individual value
    Available settings are:
    - weight_decay: Amount l2 weight penalty to add to the loss for each group.
                    Default: 0.0
    - lr: Learning rate for each group. Default: 0.005
    - optim_type: One of [GradientDescent, QNG, Adam, QuantumAdam] determining
                  the type of optimizer to use. Default: Adam"""
    var = model.var()
    param_groups = []
    lr = params.get("lr", 0.005)
    weight_decay = params.get("weight_decay", 0.0)

    # Each entry in the tuple of parameters is a parameter group
    for v in var:
        param_groups.append({"params": v})

    # if the settings in the paramfile are a list then each paramgroup
    # gets its own values
    if type(lr) == list:
        for i, l in enumerate(lr):
            param_groups[i]["lr"] = l
        lr = 5.e-3
    if type(weight_decay) == list:
        for i, w in enumerate(weight_decay):
            param_groups[i]["weight_decay"] = w
        weight_decay = 0.0



    if params.get("optim_type", "Adam") in ["QuantumAdam", "Adam"]:
        return Adam(param_groups,
                    lr=lr,
                    betas=(params.get("beta1", 0.9), params.get("beta2", 0.999)),
                    weight_decay=weight_decay)
    elif params.get("optim_type") in ["GradientDescent", "QNG"]:
        return GradientDescent(param_groups,
                               lr=lr)

    else:
        raise(RuntimeError(f"Unknown optimizer {params.get('optim_type')}"))


def get_scheduler(params, opt, steps_per_epoch):
    """Creates an scheduler based on the settings in params.
    Available settings are:
    - scheduler: One of [exponential, one_cycle] determining the type of lr_scheduler
    to use. Default: exponential
    - lr: Max learning rate for one cycle. Default: 0.005
    - anneal: Decay for exponential learning rate. Default: 1"""
    if params.get("scheduler", "exponential") == "exponential":
        decay_per_batch = params.get("anneal", 1) ** (1/steps_per_epoch)
        return ExponentialLR(opt, gamma=decay_per_batch)
    elif params.get("scheduler") == "one_cycle":
        return OneCycleLR(opt,
                          params.get("lr", 0.005),
                          epochs=params.get("n_epochs", 5),
                          steps_per_epoch=steps_per_epoch,
                          base_momentum=params.get("beta1", 0.9)-0.05,
                          max_momentum=params.get("beta1", 0.9)+0.05)
    else:
        print(f"Warning, no scheduler named {params.get('scheduler')}, defaulting to constant LR")

def get_loss_functions(params, model, data_store):
    # Create function for changing lambda over time
    lambdas = params.get("lambda", 1)
    if type(lambdas) == int or type(lambdas) == float:
        get_lambda = lambda i: lambdas if i > 0 else 1
    elif type(lambdas) == list:
        get_lambda = lambda i: lambdas[i]
    else:
        raise(ValueError(f"Lambda should be type list, float or int, not {type(lambdas)}"))

    # Create the cost functions based on parameter settings
    cost_fncs = []
    if params.get("model_type", "VQC") in ["VQC", "ClassicalNN"]:
        # MSE Classifier loss
        cost_fncs.append(CostMSE(model.var(), lam=get_lambda(0)))
    elif "INN" == params.get("model_type") and params.get("loss") == "ALL":
        # INN with all losses
        cost_fncs.append(CostGaussian(model.var(), lam=get_lambda(0)))
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(0), kernel_widths=params.get("kernel_widths", 1.0), side="input"))
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(1), kernel_widths=params.get("kernel_widths", 1.0)))
    elif "INN" == params.get("model_type") and params.get("loss") == "KL":
        # INN with KL loss
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(0), kernel_widths=params.get("kernel_widths", 1.0), side="input"))
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(1), kernel_widths=params.get("kernel_widths", 1.0)))
    elif "INN" == params.get("model_type"):
        # INN with gaussian loss
        cost_fncs.append(CostGaussian(model.var(), lam=get_lambda(0)))
    elif "QINN" in params.get("model_type") and params.get("loss") == "KL":
        # QINN with KL loss
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(0), kernel_widths=params.get("kernel_widths", 1.0), side="input"))
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(1), kernel_widths=params.get("kernel_widths", 1.0)))
    elif "QINN" in params.get("model_type") and params.get("loss") == "ALL":
        # QINN with all losses
        cost_fncs.append(CostGaussian(model.var(), lam=get_lambda(0)))
        cost_fncs.append(CostFidelity(model.var(), lam=get_lambda(1)))
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(2), kernel_widths=params.get("kernel_widths", 1.0), side="input"))
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(3), kernel_widths=params.get("kernel_widths", 1.0)))
    elif "QINN" in params.get("model_type") and params.get("loss") == "Identity_KL":
        cost_fncs.append(CostGaussian(model.var(), lam=get_lambda(0)))
        cost_fncs.append(CostIdentity(model.var(), lam=get_lambda(1), only_isp=False, log=params.get("log_loss", False)))
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(2), kernel_widths=params.get("kernel_widths", 1.0), side="input"))
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(3), kernel_widths=params.get("kernel_widths", 1.0)))
    elif "QINN" in params.get("model_type") and params.get("loss") == "Identity":
        cost_fncs.append(CostGaussian(model.var(), lam=get_lambda(0)))
        cost_fncs.append(CostIdentity(model.var(), lam=get_lambda(1), only_isp=False, log=params.get("log_loss", False)))
    elif "QINN" in params.get("model_type"):
        # QINN with gaussian loss
        cost_fncs.append(CostGaussian(model.var(), lam=get_lambda(0)))
        cost_fncs.append(CostFidelity(model.var(), lam=get_lambda(1)))
    elif params.get("model_type") in ["VAE"]:
        # Variational AutoEncoder loss
        cost_fncs.append(CostVAE(model.var(), lam=get_lambda(0)))
    else:
        raise(ValueError(f"Invalid model name {params.get('model_type')}"))

    if params.get("invariant_mass_loss", False):
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(len(cost_fncs)), kernel_widths=params.get("kernel_widths", 1.0),
                         side="input", observable=InvariantMassZ(data_store, params.get("preprocessing")), kernel="bw_kernel"))

    if params.get("transverse_momentum_loss", False):
        cost_fncs.append(CostKL(model.var(), lam=get_lambda(len(cost_fncs)), kernel_widths=params.get("kernel_widths", 1.0),
                         side="input", observable=TransverseMomentumZ(data_store, params.get("preprocessing"))))

    if params.get("identity_mass_loss", False):
        cost_fncs.append(CostIdentity(model.var(), lam=get_lambda(len(cost_fncs)), only_isp=False,
                         observable=InvariantMassZ(data_store, params.get("preprocessing"))))



    return cost_fncs

#-------------------------------------------------------------------------------
# Training

def isp_pretraining(model, data_store, params):
    cost_fnc = CostIdentity(model.var(), lam=1.0, only_isp=True)
    opt = get_optimizer(params, model)
    data_store["pretraining_loss"] = []


    for i in trange(params.get("pretraining_iterations"), desc="Pretraining ISP"):
        opt.zero_grad()
        noise = torch.randn(params.get("bs", 1024), model.n_features)
        train_loss = cost_fnc(model, noise, noise)
        train_loss.backward()
        data_store["pretraining_loss"].append(train_loss.item())
        opt.accumulate_grad()
        opt.step()
    print(f"Final pretraining loss: {data_store['pretraining_loss'][-1]}")


def train(trainloader, valloader, model, data_store, params):
    """Training loop for quantum models.
    The statistics of the training are saved in data_store.

    Available settings in params:
    - model_type: The type of model to train. Available options: VQC, QINN
    Default: VQC
    - loss: If set to "KL", uses Kullback-Leibler divergence to train the QINN
    instead of the gaussian loss. If set to "ALL", used combined gaussian and KL loss.
    - n_epochs: The number of epochs to train for. Default: 5
    - lambda: The factor between the gaussian and fidelity loss for the QINN
    or between the target and latent KL divergence if using KL loss. Default: 0.5
    - scheduler: The type of LR scheduler to use.
    Available options: exponential, one_cycle, None. Default: exponential
    - optim_type: The type of optimizer to use.
    Available options: Adam, Quantum. Default: Adam
    - lr: The base learning rate to use for the scheduler.
    Default: 0.05 for one_cycle (max_lr), 0.005 else
    - anneal: The decay factor for the exponential scheduler. Default: 1 (no decay)
    - beta1: The base momentum to use for Adam. Default: 0.9
    - beta2: The base regularisation to use for Adam. Default: 0.999
    """
    data_store["loss"] = [] # Save training loss
    data_store["accuracy"] = [] # Save training accuracy for classifiers
    data_store["fidelity"] = [] # Save fidelity for QINNs
    data_store["gaussian_loss"] = [] # Save gaussian validation loss for QINNs
    data_store["fidelity_loss"] = [] # Save fidelity validation loss for QINNs
    data_store["other_loss"] = [] # Save other validation loss for QINNs
    data_store["latent_epoch"] = [] # Save snapshots of the latent space in intervals
    data_store["samples_epoch"] = [] # Save snapshots of the input space in intervals

    cost_fncs = get_loss_functions(params, model, data_store)
    opt = get_optimizer(params, model)
    sched = get_scheduler(params, opt, len(trainloader))

    if params.get("pretraining_iterations", 0):
        isp_pretraining(model, data_store, params)

    for epoch in trange(params.get("n_epochs", 5), desc="Epoch"):
        train_loss = 0

        for i, (x, y) in enumerate(tqdm(trainloader, leave=False, desc="Batch")):

            if i % params.get("fid_loss_ratio", 1):
                fidelity_only = True
            else:
                fidelity_only = False

            x, y = x.float(), y.float()

            # Add noise to the data for stability
            if params.get("noise", 0.0) > 0.0:
                noise = params.get("noise") * torch.randn(*x.shape)
                x = torch.clamp(x + noise, 0, torch.pi)

            for j, cost_fnc in enumerate(cost_fncs):
                if fidelity_only and j != 1:
                    continue


                closure = None
                # Calculate the Fubini-Study Metric if Quantum Natural Gradient is selected

                opt.zero_grad()
                batch_loss = cost_fnc(model, x, y)
                # if batch loss is too big/unstable skip it
                if not batch_loss < 1e20:
                    print(f"Warning, loss function {j+1} of value {batch_loss} exceeds threshold")
                    continue

                train_loss += batch_loss.item()/len(trainloader)
                batch_loss.backward()

                # If target groups are enabled, get them individually for each
                # loss function
                target_groups = None
                if params.get("only_target_params", False)  or fidelity_only:
                    target_groups = cost_fnc.target_groups

                if params.get("optim_type") in ["QNG", "QuantumAdam", "QuantumNesterov"]:
                    with torch.no_grad():
                        metric_tensors = cost_fnc.metric_tensor(model, x, y).float()
                        # Add regularisation to the metric tensor
                        metric_tensors += torch.eye(*metric_tensors.shape, device=metric_tensors.device) * params.get("mt_reg", 1.e-2)

                        closure = lambda :  metric_tensors

                # Save grad for later
                opt.accumulate_grad(closure=closure, target_groups=target_groups)


            # perform optimizer step
            opt.step()
            sched.step()

            # Save snapshot of latent space and created distribution
            with torch.no_grad():
                if not ((i + len(trainloader)*epoch) % ceil((len(trainloader)*params.get("n_epochs", 5))/40)):
                    if "latent_epoch" in params.get("plots"):
                        data_store["latent_epoch"].append(model(torch.cat([x for i, (x,y) in enumerate(valloader) if (i+1)*len(x) < 2000], dim=0).float()).cpu().detach().numpy())
                    if "samples_epoch" in params.get("plots"):
                        data_store["samples_epoch"].append(rescale(model.create_samples(2000).cpu().detach().numpy(), data_store))

        tqdm.write(f"Train loss in epoch {epoch}: {train_loss}")
        model.save()

        # Validation
        with torch.no_grad():
            if params.get("model_type", "VQC") in ["VQC", "ClassicalNN"]:
                data_store["loss"].append(validate(valloader, model))
                data_store["accuracy"].append(accuracy(valloader, model))
            elif "QINN" in params.get("model_type"):
                val_ret = qinn_validate(valloader, model, cost_fncs, only_MMD=params.get("loss") == "KL")
                data_store["loss"].append(val_ret[0] + val_ret[1] + val_ret[3])
                data_store["gaussian_loss"].append(val_ret[0])
                data_store["fidelity_loss"].append(val_ret[1])
                data_store["fidelity"].append(val_ret[2])
                data_store["other_loss"].append(val_ret[3])
            elif "INN" == params.get("model_type"):
                val_ret = inn_validate(valloader, model, cost_fncs)
                data_store["loss"].append(sum(val_ret))
                data_store["gaussian_loss"].append(val_ret[0])


        loss = data_store["loss"][-1]
        tqdm.write(f"Validation loss in epoch {epoch}: {loss}")

    model.save()
