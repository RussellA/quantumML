import pennylane as qml
from pennylane import numpy as np

#---------------- ROT gate model -----------------------------------------------

def U(parameters=None):
    #Add layers
    for i in range(parameters.shape[0]): # -1):
        for j in range(parameters.shape[1]):
            qml.Rot(parameters[i, j, 0], parameters[i, j, 1], parameters[i, j, 2], wires=j)


        for j in range(parameters.shape[1]):
            if j + 1 < parameters.shape[1]:
                qml.CNOT(wires=[j,j+1])
            else:
                qml.CNOT(wires=[j,0])


def U_dagger(parameters=None):
    #Add layers
    for i in range(parameters.shape[0])[::-1]:
        for j in range(parameters.shape[1])[::-1]:
            if j + 1 < parameters.shape[1]:
                qml.CNOT(wires=[j,j+1])
            else:
                qml.CNOT(wires=[j,0])

        for j in range(parameters.shape[1])[::-1]:
            qml.Rot(-parameters[i, j, 2], -parameters[i, j, 1], -parameters[i, j, 0], wires=j)

#---------------- CROT gate model ----------------------------------------------

def U_cRot(parameters=None):
    #Add layers

    for i in range(parameters.shape[0]):
        for j in range(parameters.shape[1]):
            qml.Rot(parameters[i, j, 0], parameters[i, j, 1], parameters[i, j, 2], wires=j)


        for j in range(parameters.shape[1]):
            if j + 1 < parameters.shape[1]:
                qml.CRot(parameters[i, j, 3], parameters[i, j, 4], parameters[i, j, 5], wires=[j,j+1])
            else:
                qml.CRot(parameters[i, j, 3], parameters[i, j, 4], parameters[i, j, 5], wires=[j,0])

def U_cRot_dagger(parameters=None):
    #Add layers
    for i in range(parameters.shape[0])[::-1]:
        for j in range(parameters.shape[1])[::-1]:
            if j + 1 < parameters.shape[1]:
                qml.CRot(-parameters[i, j, 5], -parameters[i, j, 4], -parameters[i, j, 3], wires=[j,j+1])
            else:
                qml.CRot(-parameters[i, j, 5], -parameters[i, j, 4], -parameters[i, j, 3], wires=[j,0])

        for j in range(parameters.shape[1])[::-1]:
            qml.Rot(-parameters[i, j, 2], -parameters[i, j, 1], -parameters[i, j, 0], wires=j)


def P(parameters=None):
    for j in range(parameters.shape[0]):
        qml.PhaseShift(parameters[j], wires=j)

def P_dagger(parameters=None):
    for j in range(parameters.shape[0])[::-1]:
        qml.PhaseShift(-parameters[j], wires=j)


def simple_state_prep(x, offset=0):
    """Encode the state x in state preparation
    Arguments:
    - x: Input data point to encode, each dimension in the data corresponds
         to a wire on the circuit.
         Can have shape either (dim x, ) or (dim x, 3), in the first case
         Ry is used to prepare each dimension, in the second the 3 angles are
         used as a ROT-gate for state preparation.
    - offset: Determines at which wire to start the state preparation
    """
    # Start at wire offset and prepare the next x.shape[0] wires with
    # the input x.
    if x.ndim == 1:
        for i in range(x.shape[0]):
            qml.RY(x[i], wires=i+offset)
    elif x.ndim == 2 and x.shape[-1] == 3:
        for i in range(x.shape[0]):
            qml.Rot(x[i, 0], x[i, 1], x[i, 2], wires=i+offset)

def cnot_state_prep(x, offset=0):
    """Encode the state x in state preparation
    Arguments:
    - x: Input data point to encode, each dimension in the data corresponds
         to a wire on the circuit.
         Can have shape either (dim x, ) or (dim x, 3). In the first case
         Ry is used to prepare each dimension, followed by an RZ of all second
         order products embedded in cnots on each wire. In the second the 3 angles are
         used as a ROT-gate for state preparation.
    - offset: Determines at which wire to start the state preparation
    """
    # Start at wire offset and prepare the next x.shape[0] wires with
    # the input x.
    if x.ndim == 1:
        for i in range(x.shape[0]):
            qml.RY(x[i], wires=i+offset)
        for i in range(x.shape[0]):
            for j in range(i+1, x.shape[0]):
                qml.CNOT(wires=[i,j])
                qml.RZ(x[i]*x[j]/np.pi, wires=j+offset)
                qml.CNOT(wires=[i,j])

    elif x.ndim == 2 and x.shape[-1] == 3:
        for i in range(x.shape[0]):
            qml.Rot(x[i, 0], x[i, 1], x[i, 2], wires=i+offset)

def duplicate_state_prep(base_method=simple_state_prep):
    def wrapper(x, offset=0):
        base_method(x, offset=offset*2)
        base_method(x, offset=offset*2+x.shape[0])
    return wrapper


class CircuitInterface:
    """Interface class for pennylane circuits. Provides access to model
    forward and backward pass and swap tests

    Arguments:
    - use_crot: Whether to use the CROT or the regular architecture of the model
    """
    def __init__(self, params):
        if params.get("CROT", False):
            self.f = U_cRot
            self.f_inv = U_cRot_dagger

        else:
            self.f = U
            self.f_inv = U_dagger
        self.P = P
        self.P_inv = P_dagger
        self.n_dims = params["dim_x"]

        if not params.get("cnot_state_prep", False):
            self.stateprep = simple_state_prep
            self.inv_stateprep = simple_state_prep
        else:
            self.stateprep = cnot_state_prep
            self.inv_stateprep = cnot_state_prep

        if "Duplicate" in params.get("model_type", "QINN"):
            self.stateprep = duplicate_state_prep(self.stateprep)
            self.inv_stateprep = duplicate_state_prep(self.inv_stateprep)


    def circuit(self, inputs, weights=None, isp=None, phaseshift=None):
        """Run a forward pass of the model circuit and measure.
        Arguments:
        - x: Input angles for the circuit
        - weights: Model parameters for the rotation gates in the forward pass
        - isp: Inverse state preparation (not used in forward pass)
        """
        self.stateprep(inputs)
        self.f(parameters=weights)
        return [qml.expval(qml.PauliZ(wires=i)) for i in range(self.n_dims)]

    def inverse_circuit(self, inputs, weights=None, isp=None, phaseshift=None):
        """Run a backward pass of the model circuit and measure.
        Arguments:
        - x: Input angles for the backwards circuit
        - weights: Model parameters for the rotation gates in the forward pass
        - isp: Inverse state preparation for the prepared state |x> before the
        forward weights are applied in reverse
        """
        self.inv_stateprep(inputs)
        if not (isp is None):
            self.f(parameters=isp)
        if not (phaseshift is None):
            self.P_inv(phaseshift)
        self.f_inv(parameters=weights)
        return [qml.expval(qml.PauliZ(wires=i)) for i in range(self.n_dims)]

    def circuit_with_isp(self, inputs, weights=None, isp=None, phaseshift=None):
        self.stateprep(inputs)
        self.f(parameters=weights)
        if not (phaseshift is None):
            self.P(phaseshift)
        if not (isp is None):
            self.f_inv(parameters=isp)
        return [qml.expval(qml.PauliZ(wires=i)) for i in range(self.n_dims)]


    def swap_test(self, inputs, weights=None, isp=None, phaseshift=None):
        """Run a swap test for the backward and forward pass states and compare
        on the input side.
        Arguments:
        - x: Original input angles for the forward circuit (only state preparation)
        - y: Input angles for the backward circuit
        - weights: Model parameters for the rotation gates in the forward pass
        - isp: Inverse state preparation for the prepared state |y> before the
        forward weights are applied in reverse
        """
        x, y = inputs
        self.stateprep(x, offset=y.shape[0])
        self.inv_stateprep(y)
        if not (isp is None):
            self.f(parameters=isp)
        if not (phaseshift is None):
            self.P_inv(phaseshift)
        self.f_inv(parameters=weights) # Apply U_dagger to the upper q-bits (y only)

        #Swap Test
        last = x.shape[0]+y.shape[0]
        qml.Hadamard(wires=last)
        for i in range(weights.shape[1]):
            qml.CSWAP(wires=[last, i, i+y.shape[0]])
        qml.Hadamard(wires=last)
        return qml.expval(qml.PauliZ(wires=last))

    def inverse_swap_test(self, inputs, weights=None, isp=None, phaseshift=None):
        """Run a swap test for the backward and forward pass states and compare
        on the latent side.
        Arguments:
        - x: Original input angles for the forward circuit (only state preparation)
        - y: Input angles for the backward circuit
        - weights: Model parameters for the rotation gates in the forward pass
        - isp: Inverse state preparation for the prepared state |x> before the
        forward weights are applied in reverse
        """
        x, y = inputs
        self.stateprep(x, offset=y.shape[0])
        self.inv_stateprep(y)

        self.f(parameters=weights)  # Apply U to the upper q-bits (y only)
        if not (phaseshift is None):
            self.P(phaseshift)
        if not (isp is None):
            self.f_inv(parameters=isp)  # Apply U to the upper q-bits (y only)


        #Swap Test
        last = x.shape[0]+y.shape[0]
        qml.Hadamard(wires=last)
        for i in range(weights.shape[1]):
            qml.CSWAP(wires=[last, i, i+y.shape[0]])
        qml.Hadamard(wires=last)
        return qml.expval(qml.PauliZ(wires=last))
