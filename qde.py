import pennylane as qml
from pennylane import numpy
import torch
from datetime import datetime
import os
from vqc import VQC
from util import periodic_shift, block_diag_to_full, TORCH_DEVICE
from circuits import CircuitInterface
from nn import ClassicalNN
from torch.nn import Parameter
from functools import partial
from functorch import vmap, jacfwd
from math import prod

torch.pi = numpy.pi




class QINN(VQC):
    """A Quantum invertible neural network.
    See Project documentation on dropbox for details on architecture.

    Arguments:
    - device: A quantum device for executing QNodes (unused)
    - params: A dictionary of settings. Available settings are:
        * initialization: Which distribution to draw the initial network
        parameters from. Available options are uniform and normal.
        Default: normal
        * init_factor: If initialization is normal, determines the standard
        deviation of the distribution. Has no effect if initialization is
        uniform. Default: 0.2
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * name: The name of the model in plots and savefiles
        * n_shots: How many shots to use for the default quantum device
        Default: None (analytic evaluation of the circuit)
    """

    def __init__(self, devices, params):
        super().__init__(devices, params)
        self.params = params
        self.a = Parameter(torch.ones(self.n_features))
        self.b = Parameter(torch.zeros(self.n_features))
        self.c = Parameter(torch.ones(self.n_features)* params.get("init_c", 3))
        self.d = Parameter(torch.zeros(self.n_features))
        self.CI = CircuitInterface(params)
        self.jacobian_batched = vmap(jacfwd(self))
        self.metric_tensor = vmap(self.metric_tensor_single)
        self.metric_tensor_fid = vmap(self.metric_tensor_fid_single)
        self.define_model()

    def create_vmap(self, func, dev=0):
        return vmap(qml.QNode(func, self.devices[dev], interface='torch'))


    def define_model(self):
        self.circuit = self.create_vmap(self.CI.circuit)
        self.inverse_circuit = self.create_vmap(self.CI.inverse_circuit)
        self.swap_test = self.create_vmap(self.CI.swap_test, dev=1)
        self.inverse_swap_test = self.create_vmap(self.CI.inverse_swap_test, dev=1)


    #---------------------------------------------------------------------------
    # Model functions
    def global_linear_transform(self, x, a, b, rev=False, clip=False):
        """Apply a linear function to x, coefficient a and bias b."""
        if clip:
            a = torch.sigmoid(a)
            b = torch.clamp(b, torch.zeros_like(a), 1 - a)
        if not rev:
            return x*a + b
        else:
            return (x-b)/a


    def forward(self, x, rev=False, eps=1e-7):
        """Complete pass of the model."""
        if x.ndim == 1:
            x = x.view(1, x.shape[0])
        if not rev:
            inp = periodic_shift(x)
            inp = self.global_linear_transform(inp, self.a, self.b, rev=rev, clip=True)
            measurement = self.circuit(inp, weights=self.weights).float()
            return self.global_linear_transform(measurement, self.c, self.d, rev=rev)
        else:
            inp = self.global_linear_transform(x, self.c, self.d, rev=rev)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
            measurement = self.inverse_circuit(inp, weights=self.weights).float()
            out = torch.arccos(measurement) # Get the original input angle
            out = self.global_linear_transform(out, self.a, self.b, rev=rev, clip=True)
            return periodic_shift(out)


    def jac_det(self, x, rev=False, eps=1e-7):
        """Calculate the jacobian determinant of the model."""
        jac = self.jacobian_batched(x)
        return torch.linalg.det(jac.view(x.shape[0], x.shape[1], x.shape[1]))

    def fidelity(self, x, y, rev=False):
        """Calculate the fidelity between the states induced from x and y.
        latent side: rev = True
        input side:  rev = False"""
        eps = 1e-7
        if not rev:
            # Compare the data points at the latent space output side of the model
            inp1 = periodic_shift(x)
            inp1 = self.global_linear_transform(inp1, self.a, self.b, rev=False, clip=True)
            inp2 = self.global_linear_transform(y, self.c, self.d, rev=True)
            inp2 = torch.arccos(torch.clamp(inp2, -1+eps, 1-eps))
            inp = torch.stack((inp1, inp2), dim=1).float()
            return self.swap_test(inp, weights=self.weights).float()
        else:
            # Compare the data points at the target space input side of the model
            inp1 = self.global_linear_transform(y, self.c, self.d, rev=True)
            inp1 = torch.arccos(torch.clamp(inp1, -1+eps, 1-eps)) # encode measurement as an angle
            inp2 = periodic_shift(x)
            inp2 = self.global_linear_transform(inp2, self.a, self.b, rev=False, clip=True)
            inp = torch.stack((inp1, inp2), dim=1).float()
            return self.inverse_swap_test(inp, weights=self.weights)


    def metric_tensor_single(self, x, rev=False, eps=1.e-7):
        """Calculate the metric tensor of the regular model pass."""

        if not rev:
            inp = periodic_shift(x)
            inp = self.global_linear_transform(inp, self.a, self.b, rev=False, clip=True)
            qnode = qml.QNode(lambda weights: self.CI.circuit(inp, weights), self.devices[0], interface='torch')
        else:
            inp = self.global_linear_transform(x, self.c, self.d, rev=True)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
            qnode = qml.QNode(lambda weights: self.CI.inverse_circuit(inp, weights), self.devices[0], interface='torch')

        metric_tensor_fn = qml.metric_tensor(qnode, approx="block-diag", hybrid=False)
        n_params = prod(self.weights.shape)
        return metric_tensor_fn(self.weights).reshape(n_params, n_params)


    def metric_tensor_fid_single(self, x, rev=False, eps=1.e-7):
        """Calculate the metric tensor of the swap test model circuit."""
        if not rev:
            # Compare the data points at the latent space output side of the model
            inp = self.global_linear_transform(x, self.c, self.d, rev=True)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps))
            qnode = qml.QNode(lambda weights: self.CI.inverse_circuit(inp, weights), self.devices[0], interface='torch')
        else:
            # Compare the data points at the target space input side of the model
            inp = periodic_shift(x)
            inp = self.global_linear_transform(inp, self.a, self.b, rev=False, clip=True)
            qnode = qml.QNode(lambda weights: self.CI.circuit_with_isp(inp, weights), self.devices[0], interface='torch')

        metric_tensor_fn = qml.metric_tensor(qnode, approx="block-diag", hybrid=False)
        n_params = prod(self.weights.shape)
        return metric_tensor_fn(self.weights).reshape(n_params, n_params)

    def run_isp(self, x, eps=1.e-7):
        """Run only the ISP and measure the prepared state."""
        inp = self.global_linear_transform(x, self.c, self.d, rev=True)
        inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
        measurement = self.circuit(inp, weights=torch.empty((0, self.n_features, 3))).float()
        measurement = measurement[:,:self.n_features]
        return self.global_linear_transform(measurement, self.c, self.d, rev=False)


    def create_samples(self, n_samples=1, return_latent=False):
        """Create samples from the input distribution by sampling gaussian noise
        and running the model in reverse.
        Arguments:
        - n_samples: How many samples to create. Default: 1
        """
        noise = torch.randn(n_samples, self.n_features, device = TORCH_DEVICE) # use gaussian data as input
        samples = self(noise, rev=True)

        # If samples are out of range [0, pi] on target side, wrap them around
        if return_latent:
            return periodic_shift(samples), noise
        else:
            return periodic_shift(samples)


    def update(self, var):
        """Update the model parameters
        Arguments:
        - var: new model parameters"""
        self.weights = var[0]
        self.a, self.b, self.c, self.d = var[1]

    def var(self):
        """Returns model parameters"""
        return self.weights, (self.a, self.b, self.c, self.d)

    def save(self):
        """Save the model to the model directory in the ./outputs folder"""
        os.makedirs("outputs", exist_ok=True)
        os.makedirs(os.path.join("outputs", self.modelname), exist_ok=True)
        torch.save(self.state_dict(), os.path.join("outputs", self.modelname, "model.pth"))

    def load(self, path):
        """Save the model to the model from a given path
        Arguments:
        - path: The path (relative to the execution directory) to the saved model"""
        state_dict = torch.load(path)
        #Current version models
        self.load_state_dict(state_dict)


class ExtendedQINN(QINN):
    """A Quantum invertible neural network with a learnable inverse state
    preparation. See Project documentation on dropbox for details on architecture.

    Arguments:
    - device: A quantum device for executing QNodes (unused)
    - params: A dictionary of settings. Available settings are:
        * initialization: Which distribution to draw the initial network
        parameters from. Available options are uniform and normal.
        Default: normal
        * init_factor: If initialization is normal, determines the standard
        deviation of the distribution. Has no effect if initialization is
        uniform. Default: 0.2
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * name: The name of the model in plots and savefiles
        * n_shots: How many shots to use for the default quantum device
        Default: None (analytic evaluation of the circuit)
        * isp_layers: Number of quantum inverse state preparation layers.
        Default: 2
        * AnglePredictorParams: A dictionary of settings for the inverse state
        preparation NN (see ClassicalNN class in nn.py for options).
    """
    def __init__(self, device, params):
        super().__init__(device, params)
        self.isp_layers = params.get("isp_layers", 2)
        if params.get("initialization", "normal") == "normal":
            self.isp = Parameter(torch.randn(self.isp_layers, self.n_features, 3) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.isp = Parameter(torch.rand((self.isp_layers, self.n_features, 3))*2*torch.pi)

        self.params["AnglePredictorParams"]["dim_x"] = self.params["dim_x"]
        self.params["AnglePredictorParams"]["out_dim"] = 3*self.params["dim_x"]
        self.params["AnglePredictorParams"]["sigmoid"] = False

        self.angle_predictor = ClassicalNN(None, self.params["AnglePredictorParams"])


    def forward(self, x, rev=False, eps=1e-7):
        """Complete pass of the model."""
        if not rev:
            return super().forward(x, rev=rev, eps=eps)
        else:
            inp = self.global_linear_transform(x, self.c, self.d, rev=rev)
            inp = self.angle_predictor(inp)
            inp = inp.reshape(inp.shape[0], -1, 3)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
            measurement = self.inverse_circuit(inp, weights=self.weights, isp=self.isp).float()
            out = torch.arccos(measurement)
            out = self.global_linear_transform(out, self.a, self.b, rev=rev, clip=True)
            return periodic_shift(out)


    def fidelity(self, x, y, rev=False):
        """Calculate the fidelity between the states induced from x and y.
        latent side: rev = True
        input side:  rev = False"""
        eps = 1e-7
        if not rev:
            # Compare the data points at the latent space output side of the model
            inp1 = periodic_shift(x)
            inp1 = self.global_linear_transform(inp1, self.a, self.b, rev=False, clip=True)
            inp2 = self.global_linear_transform(y, self.c, self.d, rev=True)
            inp2 = self.angle_predictor(inp2)
            inp2 = inp2.reshape(inp2.shape[0], -1, 3)
            inp2 = torch.arccos(torch.clamp(inp2, -1+eps, 1-eps))
            inp = (inp1, inp2)
            return self.swap_test(inp, weights=self.weights, isp=self.isp).float()
        else:
            # Compare the data points at the target space input side of the model
            inp1 = self.global_linear_transform(y, self.c, self.d, rev=True)
            inp1 = self.angle_predictor(inp1)
            inp1 = inp1.reshape(inp1.shape[0], -1, 3)
            inp1 = torch.arccos(torch.clamp(inp1, -1+eps, 1-eps)) # encode measurement as an angle
            inp2 = periodic_shift(x)
            inp2 = self.global_linear_transform(inp2, self.a, self.b, rev=False, clip=True)
            inp = (inp1, inp2)
            return self.inverse_swap_test(inp, weights=self.weights, isp=self.isp).float()

    def metric_tensor_single(self, x, rev=False, eps=1.e-7):
        """Calculate the metric tensor of the regular model pass."""

        if not rev:
            inp = periodic_shift(x)
            inp = self.global_linear_transform(inp, self.a, self.b, rev=False, clip=True)
            qnode = qml.QNode(lambda weights, isp: self.CI.circuit(inp, weights, isp), self.devices[0], interface='torch')
        else:
            inp = self.global_linear_transform(x, self.c, self.d, rev=True)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
            inp = self.angle_predictor(inp)
            inp = inp.reshape(-1, 3)
            qnode = qml.QNode(lambda weights, isp: self.CI.inverse_circuit(inp, weights, isp), self.devices[0], interface='torch')

        metric_tensor_fn = qml.metric_tensor(qnode, approx="block-diag", hybrid=False)
        return metric_tensor_fn(self.weights, self.isp)


    def metric_tensor_fid_single(self, x, rev=False, eps=1.e-7):
        """Calculate the metric tensor of the swap test model circuit."""
        if not rev:
            # Compare the data points at the latent space output side of the model
            inp = self.global_linear_transform(x, self.c, self.d, rev=True)
            inp = self.angle_predictor(inp)
            inp = inp.reshape(-1, 3)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps))
            qnode = qml.QNode(lambda weights, isp: self.CI.inverse_circuit(inp, weights, isp), self.devices[0], interface='torch')
        else:
            # Compare the data points at the target space input side of the model
            inp = periodic_shift(x)
            inp = self.global_linear_transform(inp, self.a, self.b, rev=False, clip=True)
            qnode = qml.QNode(lambda weights, isp: self.CI.circuit_with_isp(inp, weights, isp), self.devices[0], interface='torch')

        metric_tensor_fn = qml.metric_tensor(qnode, approx="block-diag", hybrid=False)
        n_params = prod(self.weights.shape)
        return metric_tensor_fn(self.weights, self.isp)

    def run_isp(self, x, eps=1.e-7):
        """Run only the ISP and measure the prepared state."""
        inp = self.global_linear_transform(x, self.c, self.d, rev=True)
        inp = self.angle_predictor(inp)
        inp = inp.reshape(inp.shape[0], -1, 3)
        inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
        measurement = self.circuit(inp, weights=self.isp).float()
        measurement = measurement[:,:self.n_features]
        return self.global_linear_transform(measurement, self.c, self.d, rev=False)


    def update(self, var, split=None):
        """Update the model parameters
        Arguments:
        - var: New model parameters
        - split: Whether to update the forward weights (0), isp weights (1)
        or both (None)"""
        if split is None:
            super().update(var[:1])
            self.isp = var[2]
            self.angle_predictor.update(var[3])
        elif split == 0:
            super().update(var[:1])
        elif split == 1:
            self.angle_predictor.update(var[3])
            self.isp = var[2]
        else:
            raise(ValueError(f"Unknown split {split}"))

    def var(self):
        """Returns model parameters"""
        return *super().var(), self.isp, self.angle_predictor.var()


class AncillaryQINN(QINN):
    """A Quantum invertible neural network with a ancillary qubits for inverse
    state preparation.
    See Project documentation on dropbox for details on architecture.

    Arguments:
    - device: A quantum device for executing QNodes (unused)
    - params: A dictionary of settings. Available settings are:
        * initialization: Which distribution to draw the initial network
        parameters from. Available options are uniform and normal.
        Default: normal
        * init_factor: If initialization is normal, determines the standard
        deviation of the distribution. Has no effect if initialization is
        uniform. Default: 0.2
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * name: The name of the model in plots and savefiles
        * n_shots: How many shots to use for the default quantum device
        Default: None (analytic evaluation of the circuit)
        * isp_layers: Number of quantum inverse state preparation layers.
        Default: 2
        * ancillary_qubits: Number of ancillary qubits to use. Default: 1
    """
    def __init__(self, device, params):
        super().__init__(device, params)
        self.isp_layers = params.get("isp_layers", 2)
        self.n_anc = params.get("ancillary_qubits", 1)
        if params.get("initialization", "normal") == "normal":
            self.isp = Parameter(torch.randn(self.isp_layers, self.n_features+self.n_anc, 3) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.isp = Parameter(torch.rand((self.isp_layers, self.n_features+self.n_anc, 3))*torch.pi*2)

    def forward(self, x, rev=False, eps=1e-7):
        """Complete pass of the model."""
        if not rev:
            return super().forward(x, rev=rev, eps=eps)
        else:
            inp = self.global_linear_transform(x, self.c, self.d, rev=rev)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
            measurement = self.inverse_circuit(inp, weights=self.weights, isp=self.isp).float()
            out = torch.arccos(measurement)
            out = self.global_linear_transform(out, self.a, self.b, rev=rev, clip=True)
            return periodic_shift(out)


    def fidelity(self, x, y, rev=False):
        """Calculate the fidelity between the states induced from x and y.
        latent side: rev = True
        input side:  rev = False"""
        eps = 1e-7
        if not rev:
            # Compare the data points at the latent space output side of the model
            inp1 = periodic_shift(x)
            inp1 = self.global_linear_transform(inp1, self.a, self.b, rev=False, clip=True)
            inp2 = self.global_linear_transform(y, self.c, self.d, rev=True)
            inp2 = torch.arccos(torch.clamp(inp2, -1+eps, 1-eps))
            return self.swap_test(inp1, inp2, weights=self.weights, isp=self.isp).float()
        else:
            # Compare the data points at the target space input side of the model
            inp1 = self.global_linear_transform(y, self.c, self.d, rev=True)
            inp1 = torch.arccos(torch.clamp(inp1, -1+eps, 1-eps)) # encode measurement as an angle
            inp2 = periodic_shift(x)
            inp2 = self.global_linear_transform(inp2, self.a, self.b, rev=False, clip=True)
            return self.inverse_swap_test(inp1, inp2, weights=self.weights, isp=self.isp).float()

    def metric_tensor_single(self, x, rev=False, eps=1.e-7):
        """Calculate the metric tensor of the regular model pass."""

        if not rev:
            inp = periodic_shift(x)
            inp = self.global_linear_transform(inp, self.a, self.b, rev=False, clip=True)
            qnode = qml.QNode(lambda weights, isp: self.CI.circuit(inp, weights, isp), self.devices[0], interface='torch')
        else:
            inp = self.global_linear_transform(x, self.c, self.d, rev=True)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
            qnode = qml.QNode(lambda weights, isp: self.CI.inverse_circuit(inp, weights, isp), self.devices[0], interface='torch')

        metric_tensor_fn = qml.metric_tensor(qnode, approx="block-diag", hybrid=False)
        return metric_tensor_fn(self.weights, self.isp)


    def metric_tensor_fid_single(self, x, rev=False, eps=1.e-7):
        """Calculate the metric tensor of the swap test model circuit."""
        if not rev:
            # Compare the data points at the latent space output side of the model
            inp = self.global_linear_transform(x, self.c, self.d, rev=True)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps))
            qnode = qml.QNode(lambda weights, isp: self.CI.inverse_circuit(inp, weights, isp), self.devices[0], interface='torch')
        else:
            # Compare the data points at the target space input side of the model
            inp = periodic_shift(x)
            inp = self.global_linear_transform(inp, self.a, self.b, rev=False, clip=True)
            qnode = qml.QNode(lambda weights, isp: self.CI.circuit_with_isp(inp, weights, isp), self.devices[0], interface='torch')

        metric_tensor_fn = qml.metric_tensor(qnode, approx="block-diag", hybrid=False)
        n_params = prod(self.weights.shape)
        return metric_tensor_fn(self.weights, self.isp)

    def update(self, var):
        """Update the model parameters
        Arguments:
        - var: new model parameters"""
        super().update(var[:1])
        self.isp = var[2]

    def var(self):
        """Returns model parameters"""
        return *super().var(), self.isp


class AncillaryExtendedQINN(ExtendedQINN):
    """A Quantum invertible neural network with a learnable inverse state
    preparation including ancillary qubits.
    See Project documentation on dropbox for details on architecture.

    Arguments:
    - device: A quantum device for executing QNodes (unused)
    - params: A dictionary of settings. Available settings are:
        * initialization: Which distribution to draw the initial network
        parameters from. Available options are uniform and normal.
        Default: normal
        * init_factor: If initialization is normal, determines the standard
        deviation of the distribution. Has no effect if initialization is
        uniform. Default: 0.2
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * name: The name of the model in plots and savefiles
        * n_shots: How many shots to use for the default quantum device
        Default: None (analytic evaluation of the circuit)
        * isp_layers: Number of quantum inverse state preparation layers.
        Default: 2
        * AnglePredictorParams: A dictionary of settings for the inverse state
        preparation NN (see ClassicalNN class in nn.py for options).
        * ancillary_qubits: Number of ancillary qubits to use. Default: 1
    """
    def __init__(self, device, params):
        super().__init__(device, params)
        self.isp_layers = params.get("isp_layers", 2)
        self.n_anc = params.get("ancillary_qubits", 1)
        if params.get("initialization", "normal") == "normal":
            self.isp = Parameter(torch.randn(self.isp_layers, self.n_features+self.n_anc, 3) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.isp = Parameter(torch.rand((self.isp_layers, self.n_features+self.n_anc, 3))*torch.pi*2)

        self.params["AnglePredictorParams"]["dim_x"] = self.params["dim_x"]
        self.params["AnglePredictorParams"]["out_dim"] = 3*(self.params["dim_x"])
        self.params["AnglePredictorParams"]["sigmoid"] = False

        self.angle_predictor = ClassicalNN(None, self.params["AnglePredictorParams"])



class AncillaryBothSidesQINN(ExtendedQINN):
    """A Quantum invertible neural network with ancillary qubits on both sides.
    See Project documentation on dropbox for details on architecture.

    Arguments:
    - device: A quantum device for executing QNodes (unused)
    - params: A dictionary of settings. Available settings are:
        * initialization: Which distribution to draw the initial network
        parameters from. Available options are uniform and normal.
        Default: normal
        * init_factor: If initialization is normal, determines the standard
        deviation of the distribution. Has no effect if initialization is
        uniform. Default: 0.2
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * name: The name of the model in plots and savefiles
        * n_shots: How many shots to use for the default quantum device
        Default: None (analytic evaluation of the circuit)
        * isp_layers: Number of quantum inverse state preparation layers.
        Default: 2
        * AnglePredictorParams: A dictionary of settings for the inverse state
        preparation NN (see ClassicalNN class in nn.py for options).
        * ancillary_qubits: Number of ancillary qubits to use. Default: 1
    """
    def __init__(self, device, params):
        super().__init__(device, params)
        self.n_anc = params.get("ancillary_qubits", 1)
        if params.get("initialization", "normal") == "normal":
            self.weights = Parameter(torch.randn(self.n_layers, self.n_features+self.n_anc, 3) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.weights = Parameter(torch.rand((self.n_layers, self.n_features+self.n_anc, 3))*2*torch.pi)

        self.isp_layers = params.get("isp_layers", 2)
        if params.get("initialization", "normal") == "normal":
            self.isp = Parameter(torch.randn(self.isp_layers, self.n_features+self.n_anc, 3) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.isp = Parameter(torch.rand((self.isp_layers, self.n_features+self.n_anc, 3))*torch.pi*2)

        self.params["AnglePredictorParams"]["dim_x"] = self.params["dim_x"]
        self.params["AnglePredictorParams"]["out_dim"] = 3*(self.params["dim_x"])
        self.params["AnglePredictorParams"]["sigmoid"] = False

        self.angle_predictor = ClassicalNN(None, self.params["AnglePredictorParams"])


class PhaseShiftQINN(ExtendedQINN):
    """A Quantum invertible neural network with a learnable inverse state
    preparation including a phase shift.
    See Project documentation on dropbox for details on architecture.

    Arguments:
    - device: A quantum device for executing QNodes (unused)
    - params: A dictionary of settings. Available settings are:
        * initialization: Which distribution to draw the initial network
        parameters from. Available options are uniform and normal.
        Default: normal
        * init_factor: If initialization is normal, determines the standard
        deviation of the distribution. Has no effect if initialization is
        uniform. Default: 0.2
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * name: The name of the model in plots and savefiles
        * n_shots: How many shots to use for the default quantum device
        Default: None (analytic evaluation of the circuit)
        * isp_layers: Number of quantum inverse state preparation layers.
        Default: 2
        * AnglePredictorParams: A dictionary of settings for the inverse state
        preparation NN (see ClassicalNN class in nn.py for options).
    """
    def __init__(self, device, params):
        super().__init__(device, params)
        self.isp_layers = params.get("isp_layers", 2)
        if params.get("initialization", "normal") == "normal":
            self.isp = Parameter(torch.randn(self.isp_layers, self.n_features, 3) * params.get("init_factor", 0.2))
            self.phaseshift = Parameter(torch.rand(self.n_features) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.isp = Parameter(torch.rand((self.isp_layers, self.n_features, 3))*torch.pi*2)
            self.phaseshift = Parameter(torch.rand(self.n_features) * torch.pi * 2)

        self.params["AnglePredictorParams"]["dim_x"] = self.params["dim_x"]
        self.params["AnglePredictorParams"]["out_dim"] = 3*(self.params["dim_x"])
        self.params["AnglePredictorParams"]["sigmoid"] = False

        self.angle_predictor = ClassicalNN(None, self.params["AnglePredictorParams"])
        self.define_model()

    def define_model(self):
        try:
            self.circuit = self.create_vmap(partial(self.CI.circuit, phaseshift=self.phaseshift))
            self.inverse_circuit = self.create_vmap(partial(self.CI.inverse_circuit, phaseshift=self.phaseshift))
            self.swap_test = self.create_vmap(partial(self.CI.swap_test, phaseshift=self.phaseshift), dev=1)
            self.inverse_swap_test = self.create_vmap(partial(self.CI.inverse_swap_test, phaseshift=self.phaseshift), dev=1)
        except AttributeError:
            return

class DuplicateBothSidesQINN(ExtendedQINN):
    """A Quantum invertible neural network which uses two copies of the measurement
    as input for a learnable inverse state preparation.
    See Project documentation on dropbox for details on architecture.

    Arguments:
    - device: A quantum device for executing QNodes (unused)
    - params: A dictionary of settings. Available settings are:
        * initialization: Which distribution to draw the initial network
        parameters from. Available options are uniform and normal.
        Default: normal
        * init_factor: If initialization is normal, determines the standard
        deviation of the distribution. Has no effect if initialization is
        uniform. Default: 0.2
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * name: The name of the model in plots and savefiles
        * n_shots: How many shots to use for the default quantum device
        Default: None (analytic evaluation of the circuit)
        * isp_layers: Number of quantum inverse state preparation layers.
        Default: 2
    """
    def __init__(self, device, params):
        super().__init__(device, params)
        n_dups = params.get("n_duplicates", 1)
        if params.get("initialization", "normal") == "normal":
            self.weights = Parameter(torch.randn(self.n_layers, self.n_features*(1+n_dups), 3) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.weights = Parameter(torch.rand((self.n_layers, self.n_features*(1+n_dups), 3))*2*torch.pi)

        self.isp_layers = params.get("isp_layers", 2)
        if params.get("initialization", "normal") == "normal":
            self.isp = Parameter(torch.randn(self.isp_layers, self.n_features*(1+n_dups), 3) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.isp = Parameter(torch.rand((self.isp_layers, self.n_features*(1+n_dups), 3))*torch.pi*2)

        self.params["AnglePredictorParams"]["dim_x"] = self.params["dim_x"]
        self.params["AnglePredictorParams"]["out_dim"] = 3*(self.params["dim_x"])
        self.params["AnglePredictorParams"]["sigmoid"] = False

        self.angle_predictor = ClassicalNN(None, self.params["AnglePredictorParams"])

class CROTQINN(ExtendedQINN):
    """A Quantum invertible neural network with a learnable inverse state
    preparation. See Project documentation on dropbox for details on architecture.

    Arguments:
    - device: A quantum device for executing QNodes (unused)
    - params: A dictionary of settings. Available settings are:
        * initialization: Which distribution to draw the initial network
        parameters from. Available options are uniform and normal.
        Default: normal
        * init_factor: If initialization is normal, determines the standard
        deviation of the distribution. Has no effect if initialization is
        uniform. Default: 0.2
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * name: The name of the model in plots and savefiles
        * n_shots: How many shots to use for the default quantum device
        Default: None (analytic evaluation of the circuit)
        * isp_layers: Number of quantum inverse state preparation layers.
        Default: 2
        * AnglePredictorParams: A dictionary of settings for the inverse state
        preparation NN (see ClassicalNN class in nn.py for options).
    """
    def __init__(self, device, params):
        params["CROT"] = True
        super().__init__(device, params)
        if params.get("initialization", "normal") == "normal":
            self.weights = Parameter(torch.randn(self.n_layers, self.n_features, 6) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.weights = Parameter(torch.rand((self.n_layers, self.n_features, 6))*2*torch.pi)
        self.isp_layers = params.get("isp_layers", 2)
        if params.get("initialization", "normal") == "normal":
            self.isp = Parameter(torch.randn(self.isp_layers, self.n_features, 6) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.isp = Parameter(torch.rand((self.isp_layers, self.n_features, 6))*2*torch.pi)

        self.params["AnglePredictorParams"]["dim_x"] = self.params["dim_x"]
        self.params["AnglePredictorParams"]["out_dim"] = 3*self.params["dim_x"]
        self.params["AnglePredictorParams"]["sigmoid"] = False

        self.angle_predictor = ClassicalNN(None, self.params["AnglePredictorParams"])


class DuplicateQINN(ExtendedQINN):
    """A Quantum invertible neural network which uses two copies of the measurement
    as input for a learnable inverse state preparation.
    See Project documentation on dropbox for details on architecture.

    Arguments:
    - device: A quantum device for executing QNodes (unused)
    - params: A dictionary of settings. Available settings are:
        * initialization: Which distribution to draw the initial network
        parameters from. Available options are uniform and normal.
        Default: normal
        * init_factor: If initialization is normal, determines the standard
        deviation of the distribution. Has no effect if initialization is
        uniform. Default: 0.2
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * name: The name of the model in plots and savefiles
        * n_shots: How many shots to use for the default quantum device
        Default: None (analytic evaluation of the circuit)
        * isp_layers: Number of quantum inverse state preparation layers.
        Default: 2
    """
    def __init__(self, device, params):
        super().__init__(device, params)
        raise(DeprecationWarning("Warning, class DuplicateQINN is deprecated."))
        self.isp_layers = params.get("isp_layers", 2)
        if params.get("initialization", "normal") == "normal":
            self.isp = Parameter(torch.randn(self.isp_layers, self.n_features*2, 3) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.isp = Parameter(torch.rand((self.isp_layers, self.n_features*2, 3))*torch.pi*2)


    def forward(self, x, rev=False, eps=1e-7):
        """Complete pass of the model."""
        if not rev:
            return super().forward(x, rev=rev, eps=eps)
        else:
            inp = self.global_linear_transform(x, self.c, self.d, rev=rev)
            inp = torch.cat([inp, inp], dim=-1)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
            measurement = self.inverse_circuit(inp, weights=self.weights, isp=self.isp).float()
            out = torch.arccos(measurement)
            out = self.global_linear_transform(out, self.a, self.b, rev=rev, clip=True)
            return periodic_shift(out)


    def fidelity(self, x, y, rev=False):
        """Calculate the fidelity between the states induced from x and y.
        latent side: rev = True
        input side:  rev = False"""
        eps = 1e-7
        if not rev:
            # Compare the data points at the latent space output side of the model
            inp1 = periodic_shift(x)
            inp1 = self.global_linear_transform(inp1, self.a, self.b, rev=False, clip=True)
            inp2 = self.global_linear_transform(y, self.c, self.d, rev=True)
            inp2 = torch.cat([inp2, inp2], dim=-1)
            inp2 = torch.arccos(torch.clamp(inp2, -1+eps, 1-eps))
            return self.swap_test(inp1, inp2, weights=self.weights, isp=self.isp).float()
        else:
            # Compare the data points at the target space input side of the model
            inp1 = self.global_linear_transform(y, self.c, self.d, rev=True)
            inp1 = torch.cat([inp1, inp1], dim=-1)
            inp1 = torch.arccos(torch.clamp(inp1, -1+eps, 1-eps)) # encode measurement as an angle
            inp2 = periodic_shift(x)
            inp2 = self.global_linear_transform(inp2, self.a, self.b, rev=False, clip=True)
            return self.inverse_swap_test(inp1, inp2, weights=self.weights, isp=self.isp).float()


    def metric_tensor_single(self, x, rev=False, eps=1.e-7):
        """Calculate the metric tensor of the regular model pass."""

        if not rev:
            inp = periodic_shift(x)
            inp = self.global_linear_transform(inp, self.a, self.b, rev=False, clip=True)
            qnode = qml.QNode(lambda weights, isp: self.CI.circuit(inp, weights, isp), self.devices[0], interface='torch')
        else:
            inp = self.global_linear_transform(x, self.c, self.d, rev=True)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
            inp = torch.cat([inp, inp], dim=-1)
            qnode = qml.QNode(lambda weights, isp: self.CI.inverse_circuit(inp, weights, isp), self.devices[0], interface='torch')

        metric_tensor_fn = qml.metric_tensor(qnode, approx="block-diag", hybrid=False)
        return metric_tensor_fn(self.weights, self.isp)


    def metric_tensor_fid_single(self, x, rev=False, eps=1.e-7):
        """Calculate the metric tensor of the swap test model circuit."""
        if not rev:
            # Compare the data points at the latent space output side of the model
            inp = self.global_linear_transform(x, self.c, self.d, rev=True)
            inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps))
            inp = torch.cat([inp, inp], dim=-1)
            qnode = qml.QNode(lambda weights, isp: self.CI.inverse_circuit(inp, weights, isp), self.devices[0], interface='torch')
        else:
            # Compare the data points at the target space input side of the model
            inp = periodic_shift(x)
            inp = self.global_linear_transform(inp, self.a, self.b, rev=False, clip=True)
            qnode = qml.QNode(lambda weights, isp: self.CI.circuit_with_isp(inp, weights, isp), self.devices[0], interface='torch')

        metric_tensor_fn = qml.metric_tensor(qnode, approx="block-diag", hybrid=False)
        n_params = prod(self.weights.shape)
        return metric_tensor_fn(self.weights, self.isp)

    def run_isp(self, x, eps=1.e-7):
        """Run only the ISP and measure the prepared state."""
        inp = self.global_linear_transform(x, self.c, self.d, rev=True)
        inp = torch.cat([inp, inp], dim=-1)
        inp = torch.arccos(torch.clamp(inp, -1+eps, 1-eps)) # encode measurement as an angle
        measurement = self.circuit(inp, weights=self.isp).float()
        measurement = measurement[:,:self.n_features]
        return self.global_linear_transform(measurement, self.c, self.d, rev=False)


    def update(self, var):
        """Update the model parameters
        Arguments:
        - var: new model parameters"""
        super().update(var[:1])
        self.isp = var[2]

    def var(self):
        """Returns model parameters"""
        return *super().var(), self.isp
