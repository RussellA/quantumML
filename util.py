import torch
from pennylane import numpy as np
import pandas as pd
import math
import os
from pennylane.utils import _flatten
import pennylane as qml
from functools import partial

torch.concatenate = torch.cat

torch.pi = np.pi

torch.clip = torch.clamp

np.rand = partial(np.random.uniform, 0.0, 1.0)

TORCH_DEVICE = "cpu"
if torch.cuda.is_available():
    TORCH_DEVICE = "cuda"

#------------------------------------------------------------------------------
# Dataloading


def load_data(path_name, n_signal=None, n_background=None):
    """Loads data from the path_name directory"""
    for filename in os.listdir(path_name):
        if "background" in filename:
            if filename.endswith(".csv"):
                data_background = pd.read_csv(os.path.join(path_name, filename)).to_numpy()[:,1:]
            elif filename.endswith(".npy"):
                data_background = np.load(os.path.join(path_name, filename))
        if "signal" in filename:
            if filename.endswith(".csv"):
                data_signal = pd.read_csv(os.path.join(path_name, filename)).to_numpy()[:,1:]
            elif filename.endswith(".npy"):
                data_signal = np.load(os.path.join(path_name, filename))
    if not (n_background is None):
        data_background = data_background[:n_background]
    if not (n_signal is None):
        data_signal = data_signal[:n_signal]
    return data_signal, data_background

def split_data(data_signal, data_background, test_rat=0.2, train_rat=0.8):
    """Splits data and labels randomly into validation and training set"""
    signal_train, background_train = data_signal[:math.ceil(train_rat*len(data_signal))], data_background[:math.ceil(train_rat*len(data_background))]
    data_train = torch.cat([signal_train, background_train], dim=0)
    labels_train = torch.cat([torch.ones(len(signal_train)), -torch.ones(len(background_train))], dim=0)
    signal_val, background_val = data_signal[math.ceil(train_rat*len(data_signal)):], data_background[math.ceil(train_rat*len(data_background)):]
    data_val = torch.cat([signal_val, background_val], dim=0)
    labels_val = torch.cat([torch.ones(len(signal_val)), -torch.ones(len(background_val))], dim=0)
    return data_train, labels_train, data_val, labels_val


#------------------------------------------------------------------------------
# Transforms

def normalize(data_signal, data_background, data_store):
    """Applies min max scaling to the data, i.e. transforms data from
    range [a, b] -> [0, pi], saving the parameters for back transformation."""
    if not ("scales" in data_store.keys() and "shifts" in data_store.keys()):
        data_min = np.min(data_signal, axis=0), np.min(data_background, axis=0)
        mins = np.min(np.stack((data_min[0], data_min[1]), axis=0), axis=0)
        data_max = np.max(data_signal, axis=0), np.max(data_background, axis=0)
        maxs = np.max(np.stack((data_max[0], data_max[1]), axis=0), axis=0)
        # scale min of data to 0
        data_signal -= mins
        data_background -= mins

        # scale max of data to 2
        data_signal /= (maxs - mins)/2
        data_background /= (maxs - mins)/2

        # Now data is in range [-1, 1], so [0, pi] after arccos
        data_signal -= 1
        data_background -= 1

        data_store["scales"] = {np.__name__: (maxs-mins)/2, torch.__name__: torch.from_numpy((maxs-mins)/2).to(TORCH_DEVICE)}
        data_store["shifts"] = {np.__name__: mins, torch.__name__: torch.from_numpy(mins).to(TORCH_DEVICE)}
    else:
        data_signal = (data_signal - data_store["shifts"][np.__name__])/data_store["scales"][np.__name__]
        data_background  = (data_background - data_store["shifts"][np.__name__])/data_store["scales"][np.__name__]
        data_signal -= 1
        data_background -= 1
    return torch.from_numpy(np.arccos(data_signal)).to(TORCH_DEVICE), torch.from_numpy(np.arccos(data_background)).to(TORCH_DEVICE)

def pad(array, count=1):
    """Pads an array with an extra dimension at the end, i.e.
    | * * |         | * * 0 |
    | * * |  ->     | * * 0 |
                    | 0 0 1 |
    """
    ret = torch.cat((array, torch.zeros((len(array), 1), device=TORCH_DEVICE)), dim=1)
    ret = torch.cat((ret, torch.zeros((1, len(ret)+1), device=TORCH_DEVICE)), dim=0)
    ret[-1][-1] = 1
    count -= 1
    if count <= 0:
        return ret
    else:
        return pad(ret, count)

def block_diag_to_full(blocks):
    """Transforms blocks of matrices into one whole matrix.
    example: input list of matrices:
    | a b |   | e f |            | a b 0 0 |
    | c d |   | g h |     ->     | c d 0 0 |
                                 | 0 0 e f |
                                 | 0 0 g h | """
    full_length = 0

    # figure out number of dimensions for the full matrix
    for tens in blocks:
        length = math.prod(tens.shape)
        length = int(math.sqrt(length))
        full_length += length

    ind = 0
    met = torch.zeros((full_length, full_length), device=TORCH_DEVICE) # Full matrix
    for tens in blocks:
        length = math.prod(tens.shape)
        length = int(math.sqrt(length))
        # Write the blocks into the corresponding submatrix
        met[ind:ind+length, ind:ind+length] = tens.reshape(length, length)
        ind += length
    return met

def rescale(data, data_store, deprecated=False):
    """Restores data to its original scale"""
    fw =  get_framework_from_array(data)
    if not deprecated:
        return ((fw.cos(data) + 1) * data_store["scales"][fw.__name__]) + data_store["shifts"][fw.__name__]
    else:
        return (data * data_store["scales"][fw.__name__]) + data_store["shifts"][fw.__name__]


def periodic_shift(x, bound = [0, torch.pi] ):
    """Shift the input into the range of the bound (default [0, pi])"""
    s = bound[1] - bound[0]
    x = torch.where(x < bound[0], x - s * torch.floor((x + bound[0])/s), x)
    x = torch.where(x > bound[1], x - s * torch.ceil((x - bound[1])/s), x)
    return x

def carthesian_to_ring(data, rev=False):
    """Transform data from cartesian to ring coordinates"""
    fw =  get_framework_from_array(data)
    if not rev:
        x, y = data[:, 0], data[:, 1]
        r = fw.sqrt(x**2 + y**2)
        phi = fw.arcsin(x / r) + (y < 0) * fw.pi
        return fw.stack((r, phi), axis=1)
    else:
        r, phi = data[:,0], data[:,1]
        return fw.stack((r * fw.sin(phi), r * fw.cos(phi)), axis=1)

def xyz_to_ptetaphi(data, rev=False, eps=1e-15):
    fw =  get_framework_from_array(data)
    if not rev:
        px, py, pz = data[:,0::3], data[:,1::3], data[:,2::3]
        pt = fw.sqrt(px **2 + py **2)
        phi = fw.arctan2(px, py)
        Ps = fw.sqrt(fw.square(px) + fw.square(py) + fw.square(pz))
        eta = 0.5 * (fw.log(fw.clip(fw.abs(Ps + pz), eps, None)) -
                     fw.log(fw.clip(fw.abs(Ps - pz), eps, None)))
        out = np.empty(data.shape)
        out[:,0::3] = pt
        out[:,1::3] = phi
        out[:,2::3] = eta
        return out

    else:
        pt, phi, eta = data[:, 0::3], data[:, 1::3], data[:, 2::3]
        px, py = fw.cos(phi)*pt, fw.sin(phi)*pt
        pz = fw.sinh(eta) * pt
        out = fw.empty(data.shape)
        out[:,0::3] = px
        out[:,1::3] = py
        out[:,2::3] = pz
        return out

def xyz_to_ptetadeltaphi(data, rev=False):
    fw =  get_framework_from_array(data)
    if not rev:
        ptetaphi = xyz_to_ptetaphi(data, rev=False)
        delta_phi = ptetaphi[:,1::6] - ptetaphi[:,4::6]
        out = fw.empty((data.shape[0], int(5*data.shape[1]/6)))
        out[:,0::5] = ptetaphi[:,0::6]
        out[:,2::5] = ptetaphi[:,2::6]
        out[:,3::5] = ptetaphi[:,3::6]
        out[:,4::5] = ptetaphi[:,5::6]
        #delta phi
        out[:,1::5] = delta_phi
        return out
    else:
        phi2 = 2*fw.pi*fw.rand(size=(data.shape[0], int(data.shape[1]/5))) - fw.pi
        ptetaphi = fw.empty((data.shape[0], int(6*data.shape[1]/5)))
        ptetaphi[:,0::6] = data[:,0::5]
        ptetaphi[:,2::6] = data[:,2::5]
        ptetaphi[:,3::6] = data[:,3::5]
        ptetaphi[:,5::6] = data[:,4::5]

        #phi
        ptetaphi[:,1::6] = data[:,1::5] + phi2
        ptetaphi[:,4::6] = phi2
        return xyz_to_ptetaphi(ptetaphi, rev=True)


def xyz_to_ptpzdeltaphi(data, rev=False):
    fw =  get_framework_from_array(data)
    if not rev:
        ptpzphi = xyz_to_ptpzphi(data, rev=False)
        delta_phi = ptpzphi[:,1::6] - ptpzphi[:,4::6]
        out = fw.empty((data.shape[0], int(5*data.shape[1]/6)))
        out[:,0::5] = ptpzphi[:,0::6]
        out[:,2::5] = ptpzphi[:,2::6]
        out[:,3::5] = ptpzphi[:,3::6]
        out[:,4::5] = ptpzphi[:,5::6]
        #delta phi
        out[:,1::5] = delta_phi
        return out
    else:
        # Random sample of phi2
        phi2 = 2*fw.pi*fw.rand(size=(data.shape[0], int(data.shape[1]/5))) - fw.pi
        ptpzphi = fw.empty((data.shape[0], int(6*data.shape[1]/5)))
        ptpzphi[:,0::6] = data[:,0::5]
        ptpzphi[:,2::6] = data[:,2::5]
        ptpzphi[:,3::6] = data[:,3::5]
        ptpzphi[:,5::6] = data[:,4::5]

        #phi
        ptpzphi[:,1::6] = data[:,1::5] + phi2
        ptpzphi[:,4::6] = phi2
        return xyz_to_ptpzphi(ptpzphi, rev=True)


def xyz_to_ptpzphi(data, rev=False, eps=1e-15):
    fw =  get_framework_from_array(data)
    if not rev:
        px, py, pz = data[:,0::3], data[:,1::3], data[:,2::3]
        pt = fw.sqrt(px **2 + py **2)
        phi = fw.arctan2(px, py)
        Ps = fw.sqrt(fw.square(px) + fw.square(py) + fw.square(pz))
        out = np.empty(data.shape)
        out[:,0::3] = pt
        out[:,1::3] = phi
        out[:,2::3] = pz
        return out

    else:
        pt, phi, pz = data[:, 0::3], data[:, 1::3], data[:, 2::3]
        px, py = fw.cos(phi)*pt, fw.sin(phi)*pt
        out = fw.empty(data.shape)
        out[:,0::3] = px
        out[:,1::3] = py
        out[:,2::3] = pz
        return out


def xyz_to_ptetadeltaphi_ptj(data, rev=False):
    fw =  get_framework_from_array(data)
    if not rev:
        leptons = xyz_to_ptetadeltaphi(data, rev=False)
        px, py, pz = data[:,0::3], data[:,1::3], data[:,2::3]
        px_z, py_z = fw.sum(px, axis=-1, keepdims=True), fw.sum(py, axis=-1, keepdims=True)
        pt_z = fw.sqrt(px_z ** 2 + py_z ** 2)
        pt_j = -pt_z
        return fw.concatenate([leptons, pt_j], axis=-1)
    else:
        leptons = data[:, :-1]
        return xyz_to_ptetadeltaphi(leptons, rev=True)

def xyz_to_ptetaphi_ptj(data, rev=False):
    fw =  get_framework_from_array(data)
    if not rev:
        leptons = xyz_to_ptetaphi(data)
        px, py, pz = data[:,0::3], data[:,1::3], data[:,2::3]
        px_z, py_z = fw.sum(px, axis=-1, keepdims=True), fw.sum(py, axis=-1, keepdims=True)
        pt_z = fw.sqrt(px_z ** 2 + py_z ** 2)
        pt_j = -pt_z
        return fw.concatenate([leptons, pt_j], axis=-1)
    else:
        leptons = data[:, :-1]
        return xyz_to_ptetaphi(leptons, rev=True)

def get_framework_from_array(data):
    if type(data) in [torch.Tensor, torch.int, torch.float]:
        return torch
    elif type(data) in [np.array, np.ndarray, np.int32, np.float32, np.tensor, int, float]:
        return np
    else:
        raise(RuntimeError(f"Unknown framework of type {type(data)}"))

def preprocess(data, type, rev=False):
    """Performs the preprocessing specified in type"""
    if type is None:
        return data
    else:
        try:
            func = eval(type)
        except NameError as e:
            print(f"Preprocessing {type} is not defined, continuing without preprocessing.")
            return data
        return func(data, rev=rev)

def get_devices(params):
    """Create quantum devices for QNode execution"""
    extra_qubits = 0
    if "Ancillary" in params.get("model_type"):
        extra_qubits += params.get("ancillary_qubits", 1)
    elif "Duplicate" in params.get("model_type"):
        extra_qubits = params["dim_x"]*params.get("n_duplicates", 1)

    device1 = qml.device('default.qubit',
                    wires=params["dim_x"]+extra_qubits,
                    shots=params.get("n_shots", None))
    device2 = qml.device('default.qubit',
                    wires=params["dim_x"]*2+1+extra_qubits*2,
                    shots=params.get("n_shots", None))
    return device1, device2


def reconstruct_Z(events, eps=1.e-8):
    fw = get_framework_from_array(events)

    l1, l2 = events[:,:3], events[:,3:]
    E_l1, E_l2 = fw.sqrt(fw.sum(l1**2, axis=1)), fw.sqrt(fw.sum(l2**2, axis=1))
    Z = fw.empty((len(events), 6))
    Z[:,0] = E_l1 + E_l2
    Z[:,1:4] = l1 + l2
    Z[:,4] = fw.sqrt((l1+l2)[:,0]**2 + (l1+l2)[:,1]**2)
    Z[:,5] = fw.abs((E_l1 + E_l2)**2 - fw.sum((l1 + l2)**2, axis=1))
    return Z

def calculate_R(events, eps=1.e-8):
    fw = get_framework_from_array(events)

    events = xyz_to_ptetaphi(events)
    delta_eta_phi = events[:,1:3] - events[:,4:6]
    delta_R = fw.sqrt(fw.sum(delta_eta_phi**2, axis=-1))
    return fw.stack([delta_R,], axis=-1)


class InvariantMassZ(object):
    def __init__(self, data_store, preprocessing_type):
        self.data_store = data_store
        self.preprocessing_type = preprocessing_type
        self.scaling = -8300, 1/400 #shift and width of the mass distribution

    def __call__(self, x):
        x_rescaled = rescale(x, self.data_store)
        x_raw = preprocess(x_rescaled, self.preprocessing_type, rev=True)
        Z_mass = reconstruct_Z(x_raw)[:,-1]
        downscaled = (Z_mass + self.scaling[0])*self.scaling[1]
        return downscaled

class TransverseMomentumZ(object):
    def __init__(self, data_store, preprocessing_type):
        self.data_store = data_store
        self.preprocessing_type = preprocessing_type
        self.scaling = 0, 1/80 #shift and width of the pt distribution

    def __call__(self, x):
        x_rescaled = rescale(x, self.data_store)
        x_raw = preprocess(x_rescaled, self.preprocessing_type, rev=True)
        Z_pt = reconstruct_Z(x_raw)[:,-2]
        downscaled = (Z_pt + self.scaling[0])*self.scaling[1]
        return downscaled
