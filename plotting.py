from pennylane import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import os
from itertools import product
from util import rescale, preprocess, periodic_shift, reconstruct_Z, calculate_R, load_data
from matplotlib import animation, rc
import matplotlib
import torch
from itertools import combinations
from dataclasses import dataclass
from qde import *
from inn import *



plt.rc('text', usetex=True)
font = {'family' : 'normal',
        'size'   : 28}

matplotlib.rc('font', **font)

# Package missing on IPPP cluster
# plt.rc('text.latex', preamble=r"\usepackage{cancel}")

#-------------------------------------------------------------------------------
# Create all plots specified in the params file
# Usage: Use the function names of the desired plotting functions in a list
# inside the parameter yaml file, for example
# plots: ["histogram_1d", "loss_plots"]

def create_plots(model, data_signal, data_background, data_store, params):
    if "INN" in params.get("model_type", "VQC"):
        with torch.no_grad():
            # Compute latent space with entire dataset
            data_store["latent"] = model(torch.cat((data_signal, data_background), dim=0).float()).cpu().detach().numpy()
            # Sample from learned distribution, the same size as the dataset
            data_store["samples"] = model.create_samples(len(data_store["latent"])).cpu().detach().numpy()

        data_signal_complete, data_background_complete = load_data(params["data"])

        data_signal, data_background = data_signal_complete[len(data_signal):2*len(data_signal)], data_background_complete[len(data_background):2*len(data_background)]

        # Undo the normalization
        data_store["samples"] = rescale(data_store["samples"], data_store)

        # Undo preprocessing
        data_store["samples"] = preprocess(data_store["samples"], params.get("preprocessing"), rev=True)

        # Undo preprocessing on epoch plots
        if "samples_epoch" in data_store.keys():
            data_store["samples_epoch"] = [preprocess(sample, params.get("preprocessing"), rev=True) for sample in data_store["samples_epoch"]]


    for plotname in params["plots"]:
        print(f"Creating plot {plotname}")
        try:
            eval(plotname + "(model, data_signal, data_background, data_store)")
        except NameError:
            print(f"Warning, plot function {plotname} not found, skipping")

#-------------------------------------------------------------------------------
# Observables


def histogram_1d(model, data_signal, data_background, data_store, n_bins=40, plot_name="observables.pdf"):
    with PdfPages(os.path.join("outputs", model.modelname, plot_name)) as pp:
        for i, name in enumerate(model.obsnames):
            obs_true = np.concatenate((data_signal, data_background), axis=0)[:,i]
            obs_gen = data_store["samples"][:,i]
            y_true, x_true = np.histogram(obs_true, bins=n_bins, range=[np.min(obs_true), np.max(obs_true)])
            y_gen, x_gen = np.histogram(obs_gen, bins=n_bins, range=[np.min(obs_true), np.max(obs_true)])
            y_true = y_true/np.sum(y_true)
            y_gen = y_gen/np.sum(y_gen)
            hists = [(y_true, x_true), (y_gen, x_gen)]
            if "QINN" in model.modelname:
                labels = ["True", "QINN"]
            else:
                labels = ["True", "INN"]
            colors = ["#e41a1c", "#3b528b"]

            dup_last = lambda a: np.append(a, a[-1])
            fig1, axs = plt.subplots(2, 1, sharex=True, figsize=(10, 8),
                    gridspec_kw={"height_ratios" : [4, 1], "hspace" : 0.00})

            for (y, x), label, color in zip(hists, labels, colors):
                axs[0].step(x, dup_last(y), label=label, color=color,
                        linewidth=3.0, where="post")

                if label == "True": continue

                ratio = y / hists[0][0]
                ratio_isnan = np.isnan(ratio)
                ratio[ratio_isnan] = 1.

                axs[1].step(x, dup_last(ratio),
                        linewidth=3.0, where="post", color=color)


            axs[0].legend(loc="upper right", frameon=False)
            axs[0].set_ylabel("normalized")
            axs[0].set_yscale("log")

            axs[1].set_ylabel(r"$\frac{\mathrm{Model}}{\mathrm{True}}$")
            axs[1].set_yticks([0.9,1,1.1])
            axs[1].set_yscale("log")
            axs[1].set_ylim([0.1,10])
            axs[1].axhline(y=1, c="black", ls="--", lw=0.7)
            axs[1].axhline(y=1.1, c="black", ls="dotted", lw=0.5)
            axs[1].axhline(y=0.9, c="black", ls="dotted", lw=0.5)
            plt.xlabel(name)
            plt.tight_layout()
            plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
            plt.close()


def histogram_2d(model, data_signal, data_background, data_store, n_bins=40, log=True, plot_name="observables2d.pdf"):
    with PdfPages(os.path.join("outputs", model.modelname, plot_name)) as pp:
        indices = list(combinations(range(len(model.obsnames)), 2))
        obspairs = list(combinations(model.obsnames, 2))
        for (i1, i2), pair in zip(indices, obspairs):
            plt.figure(figsize=(13, 6))
            y1_true = np.concatenate((data_signal, data_background), axis=0)[:,i1]
            y2_true = np.concatenate((data_signal, data_background), axis=0)[:,i2]
            y1_gen = data_store["samples"][:,i1]
            y2_gen = data_store["samples"][:,i2]
            if "QINN" in model.modelname:
                labels = ["True", "QINN"]
            else:
                labels = ["True", "INN"]
            y1_min = np.quantile(np.concatenate((y1_true, y1_gen), axis=0), 0.01, axis=0)
            y1_max = np.quantile(np.concatenate((y1_true, y1_gen), axis=0), 0.99, axis=0)
            y2_min = np.quantile(np.concatenate((y2_true, y2_gen), axis=0), 0.01, axis=0)
            y2_max = np.quantile(np.concatenate((y2_true, y2_gen), axis=0), 0.99, axis=0)

            fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12,6))

            vmin, vmax = 1, 0

            hists = []

            for i, (y1, y2) in enumerate([(y1_true, y2_true), (y1_gen, y2_gen)]):
                hist, xedges, yedges = np.histogram2d(y1, y2, bins=[n_bins, n_bins],
                                                    density=True, range=[[y1_min, y1_max], [y2_min, y2_max]])

                vmin = min(np.min(hist), vmin)
                vmax = max(np.max(hist), vmax)
                hists.append((xedges, yedges, hist.T))

            for i, plot_set in enumerate(hists):
                ax = axes[i]

                if log:
                    vmin = max(vmin, vmax*5.e-3)
                    mesh = ax.pcolormesh(*plot_set, rasterized=True, norm=matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax, clip=True))
                else:
                    mesh = ax.pcolormesh(*plot_set, rasterized=True, vmin=vmin, vmax=vmax)
                ax.set_xlabel(pair[0])
                ax.set_ylabel(pair[1])
                if i == len(hists)-1:
                    cb_ax = fig.add_axes([1.0, 0.1, 0.02, 0.8])
                    cbar = fig.colorbar(mesh, cax=cb_ax)
                    cbar.set_label("density of events")
                ax.set_title(labels[i])
            plt.tight_layout()
            plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
            plt.close()

def plot_Z(model, data_signal, data_background, data_store, **kwargs):
    Z_signal, Z_background = reconstruct_Z(data_signal), reconstruct_Z(data_background)
    Z_data_store = {"samples": reconstruct_Z(data_store["samples"])}
    R_ll_signal, R_ll_background = calculate_R(data_signal), calculate_R(data_background)
    R_ll_data_store = {"samples": calculate_R(data_store["samples"])}


    Z_signal[:,-1] = np.sqrt(Z_signal[:,-1])
    Z_background[:,-1] = np.sqrt(Z_background[:,-1])
    Z_data_store["samples"][:,-1] = np.sqrt(Z_data_store["samples"][:,-1])

    Z_signal  = np.concatenate([Z_signal, R_ll_signal], axis=-1)
    Z_background  = np.concatenate([Z_background, R_ll_background], axis=-1)
    Z_data_store["samples"]  = np.concatenate([Z_data_store["samples"], R_ll_data_store["samples"]], axis=-1)


    @dataclass
    class FakeModel:
        modelname: str
        obsnames: list
        n_features: int
    fake_model = FakeModel(modelname=model.modelname,
                           obsnames=[r"$E_Z \; [\mathrm{GeV}]$",
                                     r"$p_{x, Z} \; [\mathrm{GeV}]$",
                                     r"$p_{y, Z} \; [\mathrm{GeV}]$",
                                     r"$p_{z, Z} \; [\mathrm{GeV}]$",
                                     r"$p_{T, Z} \; [\mathrm{GeV}]$",
                                     r"$M_Z \; [\mathrm{GeV}]$",
                                     r"$\Delta R_{\ell^+ \ell^-}$"],
                           n_features=6)
    histogram_1d(fake_model, Z_signal, Z_background, Z_data_store, plot_name="Z_reconstructed.pdf", **kwargs)


def ISP_features(model, data_signal, data_background, data_store, **kwargs):
    with torch.no_grad():
        noise = torch.randn(len(data_signal), model.n_features)
        ISP_features = model.run_isp(noise)
    fake_data_store = {"latent": ISP_features}
    latent_space(model, data_signal, data_background, fake_data_store, plot_name="ISP_features.pdf",**kwargs)



#-------------------------------------------------------------------------------
# Latent Space


def latent_space(model, data_signal, data_background, data_store, plot_name="latent.pdf", n_bins=30, log=True, eps=5.e-4):
    with PdfPages(os.path.join("outputs", model.modelname, plot_name)) as pp:
        n_dims = data_store["latent"].shape[1]
        dup_last = lambda a: np.append(a, a[-1])

        plt.figure(figsize=(n_dims*6+1,6))
        for i in range(n_dims):
            plt.subplot(1, n_dims, i+1)
            y_lat, x_lat = np.histogram(data_store["latent"][:,i], bins=n_bins, range=[-3, 3])
            y_lat = y_lat/np.sum((x_lat[1:] - x_lat[:-1]) * y_lat)
            plt.plot(x_lat, np.exp(-0.5*(x_lat**2))/np.sqrt(2*np.pi))
            plt.step(x_lat, dup_last(y_lat), linewidth=1.0, where="post")
            plt.ylim([0, 0.6])
        plt.tight_layout()
        plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
        plt.close()

        fig, axes = plt.subplots(nrows=n_dims, ncols=n_dims, figsize=(6*n_dims+2,6*n_dims))

        vmin, vmax = 1, 0
        hists = []

        for i in range(n_dims):
            axes_dim = axes[i]
            hists.append([])
            for j in range(n_dims):
                if j > i:
                    axes_dim[j].set_axis_off()
                    continue
                if i == j:
                    y1, y2 = np.random.randn(len(data_store["latent"])), np.random.randn(len(data_store["latent"]))
                else:
                    y1, y2 = data_store["latent"][:,j], data_store["latent"][:,i]
                hist, xedges, yedges = np.histogram2d(y1, y2, bins=(n_bins, n_bins),
                                                    density=True, range=[[-3, 3], [-3, 3]])
                vmin = min(np.min(hist), vmin)
                vmax = max(np.max(hist), vmax)
                hists[i].append((j, (xedges, yedges, hist.T)))

        for i in range(n_dims):
            axes_dim = axes[i]
            for j, plot_set in hists[i]:
                ax = axes_dim[j]
                if i == j:
                    ax.set_title(f"Reference")
                else:
                    if "QINN" in model.modelname:
                        ax.set_title(f"QINN")
                    else:
                        ax.set_title(f"INN")
                    ax.set_xlabel(f"Latent Dimension {i+1}")
                    ax.set_ylabel(f"Latent Dimension {j+1}")
                if log:
                    vmin = max(vmin, eps)
                    mesh = ax.pcolormesh(*plot_set, rasterized=True, norm=matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax, clip=True))
                else:
                    mesh = ax.pcolormesh(*plot_set, hist.T, rasterized=True, vmin=vmin, vmax=vmax)
                if j == n_dims - 1:
                    cb_ax = fig.add_axes([1.0, 0.1, 0.02, 0.8])
                    cbar = fig.colorbar(mesh, cax=cb_ax)
                    cbar.set_label("density of events")
        plt.tight_layout()
        plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
        plt.close()

#-------------------------------------------------------------------------------
# Epoch plots

def latent_epoch(model, data_signal, data_background, data_store, n_bins=30, log=True):
    if data_store.get("latent_epoch") is None:
        print("No epoch latent data found in data store, skipping plots")
        return
    latent_epoch1d(model, data_signal, data_background, data_store, n_bins)
    if data_store["latent_epoch"][0].shape[1] < 2:
        return
    fig = plt.figure(figsize=(13, 6))
    plt.subplot(1, 2, 2)
    y1, y2 = np.random.randn(len(data_store["latent"])), np.random.randn(len(data_store["latent"]))
    plt.xlabel(f"Optimal")
    plt.ylabel(f"Optimal")
    hist, xedges, yedges = np.histogram2d(y1, y2, bins=[n_bins, n_bins],
                                        density=True, range=[[-3, 3], [-3, 3]])
    if log:
        plt.pcolormesh(xedges, yedges, hist.T, rasterized=True, norm=matplotlib.colors.LogNorm(clip=True))
    else:
        plt.pcolormesh(xedges, yedges, hist.T, rasterized=True)


    plt.subplot(1, 2, 1)
    counts = np.zeros((n_bins, n_bins))
    counts[0,0] = 1.
    plt.title("Checkpoint 0")

    cmesh = plt.pcolormesh(xedges, yedges, counts, rasterized=True)
    plt.xlabel(f"Latent Dimension {1}")
    plt.ylabel(f"Latent Dimension {2}")

    def animate(i):
        y1, y2 = data_store["latent_epoch"][i][:,0], data_store["latent_epoch"][i][:,1]


        hist, _, _ = np.histogram2d(y1, y2, bins=[n_bins, n_bins],
                                            density=True, range=[[-3, 3], [-3, 3]])

        if log:
            hist = matplotlib.colors.LogNorm(clip=True)(hist)

        cmesh.set_array(hist.T)
        plt.title(f"Checkpoint {i}")

    frames = len(data_store["latent_epoch"])
    anim = animation.FuncAnimation(fig, animate, frames=len(data_store["latent_epoch"]), interval=10000/frames, blit=False)
    writergif = animation.PillowWriter(fps=frames/10)
    anim.save(os.path.join("outputs", model.modelname, 'latent_animation_2d.gif'), writer=writergif)

def latent_epoch1d(model, data_signal, data_background, data_store, n_bins=30):#
    fig = plt.figure(figsize=(13, 6))
    plt.subplot(1, 2, 2)
    y1 = np.random.randn(len(data_store["latent"]))
    plt.xlabel(f"Optimal")
    plt.ylabel(f"Optimal")

    dup_last = lambda a: np.append(a, a[-1])
    y_lat, x_lat = np.histogram(y1, bins=n_bins, range=[-3, 3])
    y_lat = y_lat/np.sum((x_lat[1:] - x_lat[:-1]) * y_lat)
    plt.plot(x_lat, np.exp(-0.5*(x_lat**2))/np.sqrt(2*np.pi))
    plt.step(x_lat, dup_last(y_lat), linewidth=1.0, where="post")
    plt.ylim([0, 0.6])

    plt.subplot(1, 2, 1)
    y = np.zeros(len(y1))
    plt.title("Checkpoint 0")
    y_lat, x_lat = np.histogram(y, bins=n_bins, range=[-3, 3])
    y_lat = y_lat/np.sum((x_lat[1:] - x_lat[:-1]) * y_lat)
    plt.plot(x_lat, np.exp(-0.5*(x_lat**2))/np.sqrt(2*np.pi))
    hist = plt.step(x_lat, dup_last(y_lat), linewidth=1.0, where="post")[0]
    plt.ylim([0, 0.6])
    plt.xlabel(f"Latent Dimension {1}")
    plt.ylabel(f"Latent Dimension {2}")

    def animate(i):
        y = data_store["latent_epoch"][i][:,0]

        y_lat, x_lat = np.histogram(y, bins=n_bins, range=[-3, 3])
        y_lat = y_lat/np.sum((x_lat[1:] - x_lat[:-1]) * y_lat)
        hist.set_data(x_lat, dup_last(y_lat))

        plt.title(f"Checkpoint {i}")

    frames = len(data_store["latent_epoch"])
    anim = animation.FuncAnimation(fig, animate, frames=len(data_store["latent_epoch"]), interval=10000/frames, blit=False)
    writergif = animation.PillowWriter(fps=frames/10)
    anim.save(os.path.join("outputs", model.modelname, 'latent_animation_1d.gif'), writer=writergif)


def samples_epoch(model, data_signal, data_background, data_store, n_bins=30, log=True):
    if data_store.get("samples_epoch") is None:
        print("No epoch samples data found in data store, skipping plots")
        return
    fig = plt.figure(figsize=(13, 6))
    plt.subplot(1, 2, 2)

    plt.xlabel(f"Truth")
    plt.ylabel(f"Truth")


    true_data = np.concatenate((data_signal, data_background), axis=0)
    y1, y2 = true_data[:,0], true_data[:,1]
    y1_min, y1_max = np.min(y1), np.max(y1)
    y2_min, y2_max = np.min(y2), np.max(y2)
    hist, xedges, yedges = np.histogram2d(y1, y2, bins=[n_bins, n_bins],
                                        density=True, range=[[y1_min, y1_max], [y2_min, y2_max]])


    if log:
        plt.pcolormesh(xedges, yedges, hist.T, rasterized=True, norm=matplotlib.colors.LogNorm(clip=True))
    else:
        plt.pcolormesh(xedges, yedges, hist.T, rasterized=True)

    plt.subplot(1, 2, 1)
    counts = np.zeros((n_bins, n_bins))
    counts[0,0] = 1.
    plt.title("Checkpoint 0")
    cmesh = plt.pcolormesh(xedges, yedges, counts, rasterized=True)
    plt.xlabel(model.obsnames[0])
    plt.ylabel(model.obsnames[1])

    def animate(i):
        y1, y2 = data_store["samples_epoch"][i][:,0], data_store["samples_epoch"][i][:,1]


        hist, _, _ = np.histogram2d(y1, y2, bins=[n_bins, n_bins],
                                            density=True, range=[[y1_min, y1_max], [y2_min, y2_max]])

        if log:
            hist = matplotlib.colors.LogNorm(clip=True)(hist)

        cmesh.set_array(hist.T)
        plt.title(f"Checkpoint {i}")



    frames = len(data_store["samples_epoch"])
    anim = animation.FuncAnimation(fig, animate, frames=frames, interval=10000/frames, blit=False)
    writergif = animation.PillowWriter(fps=frames/10)
    anim.save(os.path.join("outputs", model.modelname, 'observables_animation.gif'), writer=writergif)


#-------------------------------------------------------------------------------
# Classifier


def decision_regions(model, data_signal, data_background, data_store, n_points=1000, grid_res=150):
    with PdfPages(os.path.join("outputs", model.modelname, "decision_regions.pdf")) as pp:
        pixels = np.empty((grid_res, grid_res))

        for i, x in enumerate(np.linspace(0, 4, grid_res)):
            for j, y in enumerate(np.linspace(0, 4, grid_res)):
                pixels[grid_res - j -1, i] = model(np.array([x,y]))
        plt.figure(figsize=(15, 15))
        plt.imshow(pixels, cmap="bwr", alpha=0.3, extent=[0, 4, 0, 4])
        plt.scatter(data_signal[:n_points,0], data_signal[:n_points,1], color="red", alpha=0.5)
        plt.scatter(data_background[:n_points,0], data_background[:n_points,1], alpha=0.5)
        plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
        plt.close()

#-------------------------------------------------------------------------------
# Losses and training statistics


def loss_plots(model, data_signal, data_background, data_store):
    if data_store.get("loss") is None:
        print("No losses found in data store, skipping loss plots")
        return
    with PdfPages(os.path.join("outputs", model.modelname, "losses.pdf")) as pp:
        plt.figure(figsize=(15, 15))
        plt.plot(data_store["loss"], label=model.modelname.split("_")[0])
        if len(data_store.get("fidelity_loss", [])):
            plt.plot(data_store["fidelity_loss"], label=model.modelname.split("_")[0] + " fidelity loss")
        if len(data_store.get("gaussian_loss", [])):
            plt.plot(data_store["gaussian_loss"], label=model.modelname.split("_")[0] + " gaussian loss")
        if len(data_store.get("other_loss", [])):
                    plt.plot(data_store["other_loss"], label=model.modelname.split("_")[0] + " other losses")
        plt.xlabel("Epoch")
        plt.ylabel("Loss")
        plt.legend()
        plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
        plt.close()

def fidelity_plots(model, data_signal, data_background, data_store, log=False):
    if data_store.get("fidelity") is None:
        print("No fidelity found in data store, skipping fidelity plots")
        return
    with PdfPages(os.path.join("outputs", model.modelname, "fidelity.pdf")) as pp:
        plt.figure(figsize=(15, 15))
        if log:
            x = - np.log(1 - np.array(data_store["fidelity"], requires_grad=False))
            plt.ylabel(r"$- \log(1 - F)$")
        else:
            x = np.array(data_store["fidelity"], requires_grad=False)
            plt.ylabel(r"$F$")

        plt.plot(x, label=model.modelname.split("_")[0])
        plt.xlabel("Epoch")
        plt.legend()
        plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
        plt.close()

def accuracy_plots(model, data_signal, data_background, data_store):
    if data_store.get("accuracy") is None:
        print("No accuracy found in data store, skipping accuracy plots")
        return
    with PdfPages(os.path.join("outputs", model.modelname, "accuracy.pdf")) as pp:
        plt.figure(figsize=(15, 15))
        plt.plot(data_store["accuracy"], label=model.modelname.split("_")[0])
        plt.ylabel("Accuracy")
        plt.xlabel("Epoch")
        plt.legend()
        plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
        plt.close()

def ROC_curves(model, data_signal, data_background, data_store, num_points=10000):
    with PdfPages(os.path.join("outputs", model.modelname, "ROC_curves.pdf")) as pp:
        num_points = min(len(data_signal), len(data_background), num_points)
        sig_pred = np.array([model(event) for event in data_signal[:num_points]])
        back_pred = np.array([model(event) for event in data_background[:num_points]])
        min_label, max_label = min(np.min(sig_pred), np.min(back_pred)), max(np.max(sig_pred), np.max(back_pred))
        acc_sig = []
        acc_back = []
        for label in np.linspace(min_label, max_label, 50):
            acc_sig.append(np.sum(sig_pred > label)/num_points)
            acc_back.append(np.sum(back_pred < label)/num_points)
        plt.figure(figsize=(15, 15))
        plt.plot(acc_back, acc_sig, label=model.modelname.split("_")[0])
        plt.legend()
        plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
        plt.close()
