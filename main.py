import pennylane as qml
import torch
import yaml
from util import load_data, normalize, split_data, preprocess, get_devices, TORCH_DEVICE
from qde import *
from vqc import *
from inn import *
from vae import *
from nn import *
from dataloader import Dataloader
from training import train
from plotting import create_plots
import os
import shutil
import argparse

torch.set_num_threads(16)

def main(args):

    #--------------------------------------------------------------------------
    # Settings
    data_store = {}
    with open(args.paramfile) as f:
        params = yaml.load(f, Loader=yaml.FullLoader)

    #--------------------------------------------------------------------------
    # Data Preparation
    if not (args.data_store is None):
        data_store = torch.load(args.data_store)
    data_signal, data_background = load_data(params["data"],
                                            n_signal=50000,
                                            n_background=1)
    data_signal = preprocess(data_signal, params.get("preprocessing"))
    data_background = preprocess(data_background, params.get("preprocessing"))
    data_signal, data_background = normalize(data_signal, data_background, data_store)
    params["dim_x"] = data_signal.shape[-1]
    data_train, labels_train, data_val, labels_val = split_data(data_signal,
                                            data_background,
                                            params.get("test_ratio", 0.2),
                                            1 - params.get("test_ratio", 0.2))
    trainloader = Dataloader(data_train, labels_train, params.get("bs", 1024))
    valloader = Dataloader(data_val, labels_val, params.get("bs", 1024))

    #--------------------------------------------------------------------------
    # Model construction

    model = eval(params.get("model_type", "VQC") + "(get_devices(params), params)").float().to(TORCH_DEVICE)

    quantum_params = []

    print(f"Number of quantum parameters: {sum([p.numel() for i, p in enumerate(model.parameters()) if p.ndim == 3])}")
    print(f"Number of classical parameters: {sum([p.numel() for i, p in enumerate(model.parameters()) if not p.ndim == 3])}")


    print(f"Working on {os.cpu_count()} cores")

    if not (args.model is None):
        model.load(args.model)
    os.makedirs(os.path.join("outputs", model.modelname), exist_ok=True)
    shutil.copyfile(args.paramfile, os.path.join("outputs", model.modelname, "params.yaml"))

    #--------------------------------------------------------------------------
    # Training
    if not args.no_train:
        train(trainloader, valloader, model, data_store, params)


    #--------------------------------------------------------------------------
    # Plotting
    torch.save(data_store, os.path.join("outputs", model.modelname, "data_store.pth"))
    create_plots(model, data_signal, data_background, data_store, params)
    print("All done \U0001F919")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Train a Quantum Model')
    parser.add_argument('paramfile', type=str,
                        help='path to the parameter file')
    parser.add_argument('--model', type=str,
                        help='path to an existing model to load before training')
    parser.add_argument('--data_store', type=str,
                        help='path to an existing data store to load before training')
    parser.add_argument('--no_train', action='store_true', default=False,
                        help='set if you want to skip training')
    parser.add_argument('--plot_model', type=str,
                        help='plot the model in the given folder, without any training')

    args = parser.parse_args()
    if not (args.plot_model is None):
        args.model = os.path.join(args.plot_model, "model.pth")
        if os.path.exists(os.path.join(args.plot_model, "data_store.pth")):
            args.data_store = os.path.join(args.plot_model, "data_store.pth")
        args.no_train = True
        args.paramfile = os.path.join(args.plot_model, "params.yaml")
    main(args)
