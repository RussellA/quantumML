import torch
from torch.optim import _functional as F
from pennylane.utils import _flatten
import math

class GradientDescent(torch.optim.Optimizer):
    """Base Gradient Descent optimizer that builds on the torch.optim.Optimizer.
    The usage differs in that the gradients of different loss functions can be
    independently accumulated. Use loss.backward() to store the gradient in the
    parameters, then use opt.accumulate_grad() to save them in temporary storage.
    Repeat until a gradient step should be executed, by calling opt.step().
    Arguments:
    - params: List of learnable model parameters or a list of parameter groups
              each group being a dictionary containing the parameter and the
              respective settings for the group
    - defaults: Default settings for the base torch.optim.Optimizer"""
    def __init__(self, params, defaults, *args, **kwargs):
        super().__init__(params, defaults)
        self.reset_grads()

    def reset_grads(self):
        self.stored_grads = [[torch.zeros(p.shape) for p in group["params"]] for group in self.param_groups]


    def accumulate_grad(self, closure=None, target_groups=None):
        """Store the current parameter gradients in self.stored_grads
        Arguments:
        - closure: Function to compute the metric tensor. If provided, the
                   optimizer will perform quantum natural gradient descent
        - target_groups: Which parameter groups should be included in the
                         gradient accumulation"""
        metric_tensor = None

        if closure is not None:
            metric_tensor = closure()
            n_params = metric_tensor.shape[0]



            circuit_params = [p for p in self.param_groups[0]["params"]]
            circuit_grads = [p.grad for p in self.param_groups[0]["params"]]

            n_forward_weights = sum([p.numel() for p in circuit_params])

            if len(self.param_groups) > 1 and n_params > n_forward_weights:
                circuit_params += [p for p in self.param_groups[2]["params"] if (p.grad is not None)]
                circuit_grads += [p.grad if (p.grad is not None) else torch.zeros_like(p) for p in self.param_groups[2]["params"]]

            circuit_grads = [g.reshape(g.numel()) for g in circuit_grads]
            circuit_grads = torch.cat(circuit_grads, dim=0)


            grads = torch.Tensor([g.data for g in circuit_grads])

            grads = torch.linalg.solve(metric_tensor, grads)

            ind = 0
            for p in circuit_params:
                p.grad.data = grads[ind:ind+p.numel()].reshape(p.grad.data.shape)
                ind += p.numel()

        for i, group in enumerate(self.param_groups):
            if (target_groups is not None) and i not in target_groups:
                continue

            for j, p in enumerate(group["params"]):

                if p.grad is None:
                    continue

                grad = p.grad.data

                if not torch.sum(torch.isnan(grad)):
                    self.stored_grads[i][j] += grad
                else:
                    print("Warning, detected nan in gradient, skipping backpropagation")


    def step(self):
        """Perform a step w.r.t. the accumulated graidents, then reset them."""
        for i, group in enumerate(self.param_groups):
            for j, p in enumerate(group["params"]):
                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state["step"] = 0


                state["step"] += 1
                p.data.add_(- group["lr"] * self.stored_grads[i][j])

        self.reset_grads()




class Adam(GradientDescent):
    """Adam optimizer with the ability to accumulate the gradient over multiple
    loss.backwards() calls. Usage similar to GradientDescent class.
    Arguments:
    - params: List of learnable model parameters or a list of parameter groups
              each group being a dictionary containing the parameter and the
              respective settings for the group
    - defaults: Default settings for torch.optim.Adam
    - lr: Learning rate
    - betas: Tuple of two floats, determining influence of momentum and squared
             momentum in the Adam parameter update
    - eps: Regularisation parameter for Adam squared momentum
    - weight_decay: amount of l2 weight penalty to add to the loss
    - amsgrad: Whether to use the AMSGrad variant of Adam
    - maximize: Maximize loss instead of minimizing

    """
    def __init__(self, params, lr=1e-3, betas=(0.9, 0.999), eps=1e-8,
                 weight_decay=0, amsgrad=False, *, maximize: bool = False):
        if not 0.0 <= lr:
            raise ValueError("Invalid learning rate: {}".format(lr))
        if not 0.0 <= eps:
            raise ValueError("Invalid epsilon value: {}".format(eps))
        if not 0.0 <= betas[0] < 1.0:
            raise ValueError("Invalid beta parameter at index 0: {}".format(betas[0]))
        if not 0.0 <= betas[1] < 1.0:
            raise ValueError("Invalid beta parameter at index 1: {}".format(betas[1]))
        if not 0.0 <= weight_decay:
            raise ValueError("Invalid weight_decay value: {}".format(weight_decay))
        defaults = dict(lr=lr, betas=betas, eps=eps,
                        weight_decay=weight_decay, amsgrad=amsgrad, maximize=maximize)
        super(Adam, self).__init__(params, defaults)

    def __setstate__(self, state):
        super(Adam, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('amsgrad', False)
            group.setdefault('maximize', False)

    @torch.no_grad()
    def step(self):
        """Perform a step w.r.t. the accumulated graidents, then reset them."""
        for i, group in enumerate(self.param_groups):
            params_with_grad = []
            grads = []
            exp_avgs = []
            exp_avg_sqs = []
            max_exp_avg_sqs = []
            state_steps = []
            beta1, beta2 = group['betas']

            for j, p in enumerate(group['params']):
                if torch.sum(self.stored_grads[i][j]) != 0:
                    params_with_grad.append(p)
                    grads.append(self.stored_grads[i][j])

                    state = self.state[p]
                    # Lazy state initialization
                    if len(state) == 0:
                        state['step'] = torch.zeros(1)
                        # Exponential moving average of gradient values
                        state['exp_avg'] = torch.zeros_like(p, memory_format=torch.preserve_format)
                        # Exponential moving average of squared gradient values
                        state['exp_avg_sq'] = torch.zeros_like(p, memory_format=torch.preserve_format)
                        if group['amsgrad']:
                            # Maintains max of all exp. moving avg. of sq. grad. values
                            state['max_exp_avg_sq'] = torch.zeros_like(p, memory_format=torch.preserve_format)

                    exp_avgs.append(state['exp_avg'])
                    exp_avg_sqs.append(state['exp_avg_sq'])

                    if group['amsgrad']:
                        max_exp_avg_sqs.append(state['max_exp_avg_sq'])

                    # update the steps for each param group update
                    state['step'] += 1
                    # record the step after step update
                    state_steps.append(state['step'])

            F.adam(params_with_grad,
                   grads,
                   exp_avgs,
                   exp_avg_sqs,
                   max_exp_avg_sqs,
                   state_steps,
                   amsgrad=group['amsgrad'],
                   beta1=beta1,
                   beta2=beta2,
                   lr=group['lr'],
                   weight_decay=group['weight_decay'],
                   eps=group['eps'],
                   maximize=group['maximize'])
        self.reset_grads()
