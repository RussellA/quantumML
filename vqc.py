import pennylane as qml
import torch
from datetime import datetime
import os
from torch.nn import Parameter
import yaml

def circuit(x, parameters):
    """Model circuit for the VQC.
    Arguments:
    - x: Input angles for the quantum model
    - parameters: Model parameters of the VQC"""

    #State Preparation
    for i in range(parameters.shape[1]):
        qml.RY(x[i], wires=i)
    #Add layers
    for i in range(parameters.shape[0]):
        for j in range(parameters.shape[1]):
            qml.Rot(parameters[i, j, 0], parameters[i, j, 1], parameters[i, j, 2], wires=j)

        for j in range(parameters.shape[1]):
            if j + 1 < parameters.shape[1]:
                qml.CNOT(wires=[j,j+1])
            else:
                qml.CNOT(wires=[j,0])
    #For measurement apply Pauli-Z to first qubit
    return qml.expval(qml.PauliZ(wires=0))

def pin_x(x, circ, device):
    """Creates a pennylane QNode with only the model parameters as an input
    predicting a fixed datapoint x. Used for metric tensor calculations.
    Arguments:
    - x: Input data for the model
    - circ: Model circuit function
    - device: Quantum device to compute the QNode on"""
    x.requires_grad = False
    return qml.QNode(lambda parameters: circ(x, parameters), device)

class VQC(torch.nn.Module):
    """A variational quantum classifier
    See Project documentation on dropbox for details on architecture.

    Arguments:
    - device: The device on which the model circuit is being executed
    - params: A dictionary of settings. Available settings are:
        * initialization: Which distribution to draw the initial network
        parameters from. Available options are uniform and normal.
        Default: normal
        * init_factor: If initialization is normal, determines the standard
        deviation of the distribution. Has no effect if initialization is
        uniform. Default: 0.2
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * name: The name of the model in plots and savefiles
    """
    def __init__(self, device, params):
        super().__init__()
        self.n_features = params["dim_x"]
        self.n_layers = params.get("n_layers", 2)
        if params.get("initialization", "normal") == "normal":
            self.weights = Parameter(torch.randn(self.n_layers, self.n_features, 3) * params.get("init_factor", 0.2))
        elif params.get("initialization") == "uniform":
            self.weights = Parameter(torch.rand((self.n_layers, self.n_features, 3))*2*torch.pi)

        self.devices = device
        self.bias = Parameter(torch.zeros(1))
        self.modelname = params["name"] + "_" + datetime.now().strftime("%m%d_%H%M")

        meta_path = os.path.join(params["data"], "meta.yaml")
        if not os.path.exists(meta_path):
            self.obsnames = [fr"$x_{i+1}$" for i in range(self.n_features)]
        else:
            with open(meta_path) as f:
                metadata = yaml.load(f, Loader=yaml.FullLoader)
                self.obsnames = metadata.get("observable_names", [fr"$x_{i+1}$" for i in range(self.n_features)])

    def forward(self, x):
        # If different weights are provided, use them for the forward pass,
        # else use the current model weights
        return self.circuit(x, self.weights) + self.bias

    def update(self, var):
        """Update model parameters"""
        self.weights, self.bias = var[0], var[1]

    def var(self):
        """Returns model parameters"""
        return self.weights, self.bias

    def save(self):
        """Save model"""
        os.makedirs("outputs", exist_ok=True)
        os.makedirs(os.path.join("outputs", self.modelname), exist_ok=True)
        torch.save(self.state_dict(), os.path.join("outputs", self.modelname, "model.pth"))

    def load(self, path):
        """Load model"""
        state_dict = torch.load(path)
        self.load_state_dict(state_dict)
