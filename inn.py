import torch
from torch import nn
from datetime import datetime
import sys
import os
from util import TORCH_DEVICE
sys.path.append("/home/russell/FrEIA/")
import FrEIA.framework as ff
import FrEIA.modules as fm
import yaml

class INN(nn.Module):
    def __init__(self, devices, params):
        super().__init__()
        self.n_features = params["dim_x"]
        self.modelname = params["name"] + "_" + datetime.now().strftime("%m%d_%H%M")
        self.obsnames = params.get("observable_names", [fr"$x_{i+1}$" for i in range(self.n_features)])
        self.hidden_dim = params.get("hidden_dim", 32)
        self.inn = self.define_model(params)

        meta_path = os.path.join(params["data"], "meta.yaml")
        if not os.path.exists(meta_path):
            self.obsnames = [fr"$x_{i+1}$" for i in range(self.n_features)]
        else:
            with open(meta_path) as f:
                metadata = yaml.load(f, Loader=yaml.FullLoader)
                self.obsnames = metadata.get("observable_names", [fr"$x_{i+1}$" for i in range(self.n_features)])


    def define_model(self, params):
        def subnet_fc(dims_in, dims_out):
            hidden = []
            for i in range(params.get("subnet_depth", 3)-2):
                hidden.append(nn.Linear(self.hidden_dim, self.hidden_dim))
                hidden.append(nn.ReLU())

            return nn.Sequential(
                nn.Linear(dims_in, self.hidden_dim),
                nn.ReLU(),
                *hidden,
                nn.Linear(self.hidden_dim, dims_out)
            )

        inn = ff.SequenceINN(self.n_features)
        for k in range(params.get("n_blocks", 5)):
            if params.get("spline_flow", True):
                inn.append(fm.CubicSplineBlock, subnet_constructor=subnet_fc, permute_soft=params.get("permute_soft", True),
                           num_bins=params.get("n_bins", 60), bounds_init=params.get("bounds_init", 10.))
            else:
                inn.append(fm.AllInOneBlock, subnet_constructor=subnet_fc, permute_soft=params.get("permute_soft", True))

        return inn.float()

    def clip_input_coefficients(self):
        pass

    def forward(self, x, rev=False):
        out, jac = self.inn(x, rev=rev)
        self.last_jac = jac
        return out

    def jac_det(self, x, rev=False, use_cached=True):
        if use_cached:
            return torch.exp(self.last_jac)
        else:
            out, jac = self.inn(x, rev=rev)
            return torch.exp(jac)

    def fidelity(self, x, y, rev=False):
        return torch.ones(x.shape[0])

    def metric_tensor(self, x, rev=False):
        raise(RuntimeError("Cannot compute the metric tensor of classical networks"))

    def metric_tensor_fid(self, x, rev=False):
        raise(RuntimeError("Cannot compute the metric tensor of classical networks"))

    def run_isp(self, x, eps=1.e-7):
        raise(RuntimeError("Classical INN has no state preparation"))


    def create_samples(self, n_samples=1, return_latent=False):
        """Create samples from the input distribution by sampling gaussian noise
        and running the model in reverse.
        Arguments:
        - n_samples: How many samples to create. Default: 1
        """
        noise = torch.randn(n_samples, self.n_features, device = TORCH_DEVICE) # use gaussian data as input
        samples = self(noise, rev=True)

        # If samples are out of range [0, pi] on target side, wrap them around
        if return_latent:
            return samples, noise
        else:
            return samples

    def update(self, var):
        """Update the model parameters
        Arguments:
        - var: new model parameters"""
        for p, q in zip(self.parameters(), var):
            p.data = q.data

    def var(self):
        """Returns model parameters"""
        return list(self.parameters())

    def save(self):
        """Save the model to the model folder in the ./outputs directory"""
        os.makedirs("outputs", exist_ok=True)
        os.makedirs(os.path.join("outputs", self.modelname), exist_ok=True)
        torch.save(self.state_dict(), os.path.join("outputs", self.modelname, "model.pth"))

    def load(self, path):
        """Save the model to the model from a given path
        Arguments:
        - path: The path (relative to the execution directory) to the saved model"""
        state_dict = torch.load(path)
        self.load_state_dict(state_dict)
