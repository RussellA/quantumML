import torch
from datetime import datetime
import os
from torch import nn

class ClassicalNN(nn.Module):
    """A classical Neural Network implementation for classification.
    The nonlinearity used is LeakyReLU with a factor of 0.1.
    The model outputs are automatically scaled to a range of [-1, 1],
    using the sigmoid function.

    Arguments:
    - device: Pennylane device for quantum calculations (unused)
    - params: A dictionary of settings. Available settings are:
        * n_layers: The number of layers in the network (Excluding the
        final one). Default: 2
        * hidden_dim: The dimension of each hidden layer. Default: 3
        * sigmoid: Whether to apply the sigmoid function at the end of the
                   forward pass (for computing class labels). Default: True
        * out_dim: number of output dimensions. Default: 1
        * name: The name of the model in plots and savefiles
    """
    def __init__(self, device, params):
        super().__init__()
        self.n_features = params["dim_x"]
        self.n_layers = params.get("n_layers", 2)
        self.hidden_dim = params.get("hidden_dim", 3)
        self.out_dim = params.get("out_dim", 1)
        self.sigmoid = params.get("sigmoid", True)
        self.layers = [nn.Linear(self.n_features, self.hidden_dim)]
        self.layers += [nn.Linear(self.hidden_dim, self.hidden_dim)] * (self.n_layers -1)
        self.layers += [nn.Linear(self.hidden_dim, self.out_dim)]
        self.layers = nn.Sequential(*self.layers)


        self.modelname = params["name"] + "_" + datetime.now().strftime("%m%d_%H%M")

    def forward(self, x):
        """Forward pass of the model. Adds a mapping from [-inf, inf] to [-1, 1]
        if sigmoid is True
        Arguments:
        - x: Input to the model"""
        out = self.layers[0](x)
        for l in self.layers[1:]:
            out = torch.nn.functional.leaky_relu(out, negative_slope=0.1)
            out = l(out)

        if self.sigmoid:
            out = torch.nn.functional.sigmoid(out)
            out = (out - 0.5)*2

        return out

    def update(self, var):
        """Update the model parameters
        Arguments:
        - var: new model parameters"""
        for p, q in zip(self.parameters(), var):
            p.data = q.data

    def var(self):
        """Returns the model parameters"""
        return list(self.parameters())

    def save(self):
        """Save the model to the model folder in the ./outputs directory"""
        os.makedirs("outputs", exist_ok=True)
        os.makedirs(os.path.join("outputs", self.modelname), exist_ok=True)
        torch.save(self.state_dict(), os.path.join("outputs", self.modelname, "model.pth"))

    def load(self, path):
        """Save the model to the model from a given path
        Arguments:
        - path: The path (relative to the execution directory) to the saved model"""
        state_dict = torch.load(path)
        self.load_state_dict(state_dict)
