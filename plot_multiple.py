from pennylane import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib
import os
import yaml
import sys
from util import load_data, get_devices, rescale, preprocess, reconstruct_Z, calculate_R, normalize
from qde import *
from vqc import *
from inn import *
from vae import *
from nn import *
import torch
import os
import pennylane as qml
from itertools import combinations
from dataclasses import dataclass

plt.rc('text', usetex=True)



def create_multiple(models, data_signal, data_background, data_stores, paramcards, plot_params):
    for model, data_store, params in zip(models, data_stores, paramcards):
        if "INN" in params.get("model_type", "VQC"):
            with torch.no_grad():

                data_signal_preproc = preprocess(data_signal, params.get("preprocessing"), rev=False)
                data_background_preproc = preprocess(data_background, params.get("preprocessing"), rev=False)

                data_signal_torch, data_background_torch = normalize(data_signal_preproc, data_background_preproc, data_store)

                # Compute latent space with entire dataset
                data_store["latent"] = model(torch.cat((data_signal_torch, data_background_torch), dim=0).float()).cpu().detach().numpy()
                # Sample from learned distribution, the same size as the dataset
                data_store["samples"] = model.create_samples(len(data_signal)).cpu().detach().numpy()

            # Undo the normalization
            data_store["samples"] = rescale(data_store["samples"], data_store)

            # Undo preprocessing
            data_store["samples"] = preprocess(data_store["samples"], params.get("preprocessing"), rev=True)

            # Undo preprocessing on epoch plots
            if "samples_epoch" in data_store.keys():
                data_store["samples_epoch"] = [preprocess(sample, params.get("preprocessing"), rev=True) for sample in data_store["samples_epoch"]]


    for plotname in plot_params["plots"]:
        print(f"Creating plot {plotname}")
        try:
            eval(plotname + "(models, data_signal, data_background, data_stores, plot_params)")
        except NameError as e:
            print(f"Warning, plot function {plotname} not found, skipping.", e)


#-------------------------------------------------------------------------------
# Observables


def histogram_1d(models, data_signal, data_background, data_stores, plot_params, n_bins=40, plot_name="observables.pdf"):
    with PdfPages(os.path.join("outputs", "paper_plots", "plots_only", plot_params["plotname"], plot_name)) as pp:
        for i, name in enumerate(plot_params["obsnames"]):
            obs_true = np.concatenate((data_signal, data_background), axis=0)[:,i]
            obs_gens = [data_store["samples"][:,i] for data_store in data_stores]
            y_true, x_true = np.histogram(obs_true, bins=n_bins, range=[np.min(obs_true), np.max(obs_true)])
            gen_bins = [np.histogram(obs_gen, bins=n_bins, range=[np.min(obs_true), np.max(obs_true)]) for obs_gen in obs_gens]
            y_true = y_true/np.sum(y_true)
            hists = [(y_true, x_true)]
            hists.extend([(gen[0]/np.sum(gen[0]), gen[1]) for gen in gen_bins])
            labels = ["True"] + plot_params["modellabels"]
            colors = ["#e41a1c"] + plot_params["modelcolors"]
            linestyles = ["solid"] + plot_params.get("modellinestyles", ["solid"]*len(models))
            alphas = [1.0] + plot_params.get("alpha", [1.0]*len(models))

            dup_last = lambda a: np.append(a, a[-1])
            fig1, axs = plt.subplots(2, 1, sharex=True, figsize=(10, 8),
                    gridspec_kw={"height_ratios" : [4, 1], "hspace" : 0.00})

            for (y, x), label, color, linestyle, alpha in zip(hists, labels, colors, linestyles, alphas):
                axs[0].step(x, dup_last(y), label=label, color=color,
                        linewidth=3.0, linestyle=linestyle, alpha=alpha, where="post")

                if label == "True": continue

                ratio = y / hists[0][0]
                ratio_isnan = np.isnan(ratio)
                ratio[ratio_isnan] = 1.

                axs[1].step(x, dup_last(ratio),
                        linewidth=3.0, where="post", color=color,
                        linestyle=linestyle, alpha=alpha)


            axs[0].legend(loc="upper right", frameon=False)
            axs[0].set_ylabel("normalized")
            axs[0].set_yscale("log")

            axs[1].set_ylabel(r"$\frac{\mathrm{Model}}{\mathrm{True}}$")
            axs[1].set_ylim([0.5,1.5])
            axs[1].set_yticks([0.75,1.25])
            axs[1].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
            axs[1].get_yaxis().set_minor_formatter(matplotlib.ticker.NullFormatter())
            axs[1].axhline(y=1, c="black", ls="--", lw=0.7)
            axs[1].axhline(y=1.25, c="black", ls="dotted", lw=0.5)
            axs[1].axhline(y=0.75, c="black", ls="dotted", lw=0.5)
            plt.xlabel(name)
            plt.tight_layout()
            plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
            plt.close()


def histogram_2d(models, data_signal, data_background, data_stores, plot_params, n_bins=40, log=True, ranges=None, plot_name="observables2d.pdf"):
    add_reference = plot_params.get("add_reference", False)
    with PdfPages(os.path.join("outputs", "paper_plots", "plots_only", plot_params["plotname"], plot_name)) as pp:
        indices = list(combinations(range(len(plot_params["obsnames"])), 2))
        obspairs = list(combinations(plot_params["obsnames"], 2))
        for (i1, i2), pair in zip(indices, obspairs):
            y1_true = np.concatenate((data_signal, data_background), axis=0)[:,i1]
            y2_true = np.concatenate((data_signal, data_background), axis=0)[:,i2]
            y1_gen = [data_store["samples"][:,i1] for data_store in data_stores]
            y2_gen = [data_store["samples"][:,i2] for data_store in data_stores]
            labels = plot_params["modellabels"]
            if add_reference:
                labels = ["Reference"] + labels

            if ranges is None:
                y1_min = np.quantile(np.concatenate((y1_true, *y1_gen), axis=0), 0.002, axis=0)
                y1_max = np.quantile(np.concatenate((y1_true, *y1_gen), axis=0), 0.998, axis=0)
                y2_min = np.quantile(np.concatenate((y2_true, *y2_gen), axis=0), 0.002, axis=0)
                y2_max = np.quantile(np.concatenate((y2_true, *y2_gen), axis=0), 0.998, axis=0)
            else:
                y1_min, y1_max = ranges[0]
                y2_min, y2_max = ranges[1]

            fig, axes = plt.subplots(nrows=1, ncols=len(models)+add_reference, figsize=(6*(len(models)+add_reference),6))

            vmin, vmax = 1, 0

            y_pairs =  [(y1, y2) for y1, y2 in zip(y1_gen, y2_gen)]

            if add_reference:
                y_pairs = [(y1_true, y2_true)] + y_pairs

            hists = []

            for i, (y1, y2) in enumerate(y_pairs):
                hist, xedges, yedges = np.histogram2d(y1, y2, bins=[n_bins, n_bins],
                                                    density=True, range=[[y1_min, y1_max], [y2_min, y2_max]])

                vmin = min(np.min(hist), vmin)
                vmax = max(np.max(hist), vmax)
                hists.append((xedges, yedges, hist.T))

            vmax = min(vmax, plot_params.get("vmax", 4.e-1))

            for i, plot_set in enumerate(hists):
                if len(hists) > 1:
                    ax = axes[i]
                else:
                    ax = axes

                if log:
                    vmin = max(vmin, vmax*5.e-3)
                    mesh = ax.pcolormesh(*plot_set, rasterized=True, norm=matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax, clip=True))
                else:
                    mesh = ax.pcolormesh(*plot_set, rasterized=True, vmin=vmin, vmax=vmax)
                ax.set_xlabel(pair[0])
                ax.set_ylabel(pair[1])
                if i == len(hists)-1:
                    cb_ax = fig.add_axes([1.0, 0.1, 0.02, 0.8])
                    cbar = fig.colorbar(mesh, cax=cb_ax)
                    cbar.set_label("density of events")
                ax.set_title(labels[i])
            plt.tight_layout()
            plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
            plt.close()

def plot_Z(models, data_signal, data_background, data_stores, plot_params,  **kwargs):
    Z_signal, Z_background = reconstruct_Z(data_signal), reconstruct_Z(data_background)
    Z_data_stores = [{"samples": reconstruct_Z(data_store["samples"])} for data_store in data_stores]
    R_ll_signal, R_ll_background = calculate_R(data_signal), calculate_R(data_background)
    R_ll_data_stores = [{"samples": calculate_R(data_store["samples"])} for data_store in data_stores]



    Z_signal[:,-1] = np.sqrt(Z_signal[:,-1])
    Z_background[:,-1] = np.sqrt(Z_background[:,-1])

    for Z_data_store, R_ll_data_store in zip(Z_data_stores, R_ll_data_stores):
        Z_data_store["samples"][:,-1] = np.sqrt(Z_data_store["samples"][:,-1])
        Z_data_store["samples"] = np.concatenate([Z_data_store["samples"], R_ll_data_store["samples"]], axis=-1)

    Z_signal  = np.concatenate([Z_signal, R_ll_signal], axis=-1)
    Z_background  = np.concatenate([Z_background, R_ll_background], axis=-1)

    fake_params = {
        "obsnames": [r"$E_Z \; [\mathrm{GeV}]$",
                 r"$p_{x, Z} \; [\mathrm{GeV}]$",
                 r"$p_{y, Z} \; [\mathrm{GeV}]$",
                 r"$p_{z, Z} \; [\mathrm{GeV}]$",
                 r"$p_{T, Z} \; [\mathrm{GeV}]$",
                 r"$M_Z \; [\mathrm{GeV}]$",
                 r"$\Delta R_{\ell^+ \ell^-}$"]
    }

    for key, value in plot_params.items():
        if key != "obsnames":
            fake_params[key] = value

    histogram_1d(models, Z_signal, Z_background, Z_data_stores, fake_params, plot_name="Z_reconstructed.pdf", **kwargs)


def ISP_features(models, data_signal, data_background, data_stores, plot_params, plot_name="ISP_features.pdf", **kwargs):
    n_dims = data_stores[0]["latent"].shape[1]
    with torch.no_grad():
        noise = torch.randn(len(data_signal), n_dims)
        fake_ds = [{"samples": model.run_isp(noise).detach().cpu().numpy()} for model in models]
    fake_params = {
        "obsnames": [f"Latent Feature {i+1}" for i in range(n_dims)],
        "vmax": 4.e-1,
        "add_reference": False
    }
    for key, value in plot_params.items():
        if key not in fake_params.keys():
            fake_params[key] = value
    histogram_2d(models, data_signal, data_background, fake_ds, fake_params, plot_name=plot_name, ranges=[[-5, 5], [-5, 5]], **kwargs)


def latent_space(models, data_signal, data_background, data_stores, plot_params, plot_name="latent.pdf", **kwargs):
    n_dims = data_stores[0]["latent"].shape[1]

    fake_params = {
        "obsnames": [f"Latent Dimension {i+1}" for i in range(n_dims)],
        "vmax": 4.e-1,
    }

    for key, value in plot_params.items():
        if key not in fake_params.keys():
            fake_params[key] = value
    fake_ds = [{"samples": data_store["latent"]} for data_store in data_stores]
    data_signal_fake = np.random.randn(len(data_signal), n_dims)
    data_background_fake = np.random.randn(len(data_background), n_dims)


    histogram_2d(models, data_signal_fake, data_background_fake, fake_ds, fake_params, plot_name=plot_name, ranges=[[-3, 3], [-3, 3]], **kwargs)

#-------------------------------------------------------------------------------
# Losses and training statistics


def loss_plots(models, data_signal, data_background, data_stores, plot_params, eps=1.e-4):
    with PdfPages(os.path.join("outputs", "paper_plots", "plots_only", plot_params["plotname"], "losses.pdf")) as pp:
        plt.figure(figsize=(15, 15))
        if plot_params.get("loss_scale", "linear") == "log":
            l_min = 0
            for ds in data_stores:
                l_min = min(np.min(ds["loss"]), l_min)
            for ds in data_stores:
                ds["loss"] -= l_min - eps
            plt.yscale("log")
        for i, data_store in enumerate(data_stores):
            if data_store.get("loss") is None:
                print("No losses found in data store, skipping loss plots")
                continue
            plt.plot(data_store["loss"],
                     label=plot_params["modellabels"][i],
                     color=plot_params["modelcolors"][i],
                     linestyle=plot_params.get("modellinestyles", ["solid"]*len(models))[i],
                     alpha=plot_params.get("alpha", [1.0]*len(models))[i])

        plt.xlabel("Epoch")
        plt.ylabel(r"Loss")
        plt.legend()
        plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
        plt.close()

def fidelity_plots(models, data_signal, data_background, data_stores, plot_params):
    with PdfPages(os.path.join("outputs", "paper_plots", "plots_only", plot_params["plotname"], "fidelity.pdf")) as pp:
        plt.figure(figsize=(15, 15))
        for i, data_store in enumerate(data_stores):
            if data_store.get("fidelity") is None:
                print("No losses found in data store, skipping loss plots")
                continue
            plt.plot(data_store["fidelity"],
                     label=plot_params["modellabels"][i],
                     color=plot_params["modelcolors"][i],
                     linestyle=plot_params.get("modellinestyles", ["solid"]*len(models))[i],
                     alpha=plot_params.get("alpha", [1.0]*len(models))[i])
        plt.xlabel("Epoch")
        plt.ylabel(r"$F$")
        plt.legend()
        plt.savefig(pp, bbox_inches="tight", format="pdf", pad_inches=0.05)
        plt.close()



def main():

    with open(sys.argv[1]) as f:
        params = yaml.load(f, Loader=yaml.FullLoader)

    font = {'family' : 'normal',
            'size'   : params.get("fontsize", 36)}

    matplotlib.rc('font', **font)
    # load test data
    data_signal, data_background = load_data(params["data"])
    data_signal_test, data_background_test = data_signal[50000:100000], data_background[1:2]


    meta_path = os.path.join(params["data"], "meta.yaml")
    if not os.path.exists(meta_path):
        params["obsnames"] = [fr"$x_{i+1}$" for i in range(data_signal.shape[-1])]
    else:
        with open(meta_path) as f:
            metadata = yaml.load(f, Loader=yaml.FullLoader)
            params["obsnames"] = metadata.get("observable_names", [fr"$x_{i+1}$" for i in range(data_signal.shape[-1])])


    # load models
    models = []
    data_stores = []
    paramcards = []
    for modelname in params["models"]:
        with open(os.path.join("outputs", "paper_plots", "folders_with_models", modelname, "params.yaml")) as f:
            model_params = yaml.load(f, Loader=yaml.FullLoader)
            tmp = preprocess(data_background, model_params.get("preprocessing"), rev=False)
            model_params["dim_x"] = tmp.shape[-1]
            paramcards.append(model_params)

        model = eval(model_params.get("model_type", "VQC") + "(get_devices(model_params), model_params)").float()
        model.load(os.path.join("outputs", "paper_plots", "folders_with_models", modelname, "model.pth"))
        models.append(model)
        data_store = torch.load(os.path.join("outputs", "paper_plots", "folders_with_models", modelname, "data_store.pth"))
        data_stores.append(data_store)

    os.makedirs("outputs", exist_ok=True)
    os.makedirs(os.path.join("outputs", "paper_plots", "plots_only", params["plotname"]), exist_ok=True)
    create_multiple(models, data_signal, data_background, data_stores, paramcards, params)


if __name__ == "__main__":

    main()
