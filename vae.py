import torch
from datetime import datetime
import os
from util import periodic_shift
from torch import nn

class Subnet(nn.Module):
    def __init__(self, dim_in, dim_out, n_layers, hidden_dim):
        super().__init__()
        self.n_features = dim_in
        self.n_layers = n_layers
        self.hidden_dim = hidden_dim
        self.n_params = self.hidden_dim*self.n_features + self.hidden_dim * dim_out + (self.n_layers - 1)*(self.hidden_dim**2)
        self.layers =  nn.Sequential([nn.Linear(self.n_features, self.hidden_dim)] + \
                        [nn.Linear(self.hidden_dim, self.hidden_dim)] * (self.n_layers -1) + \
                        [nn.Linear(self.hidden_dim, out_dim)])

    def __call__(self, x, parameters):
        weights, bias = parameters
        out = self.layers[0](x)
        for l in self.layers[1:]:
            out = torch.nn.functional.relu(out)
            out = l(out)
        return out

    def update(self, var):
        self.load_state_dict(var)

    def var(self):
        return self.state_dict()



class VAE(nn.Module):
    def __init__(self, device, params):
        super.__init__()

        #architecture
        self.dim_x          = params['dim_x']
        self.hidden_dim     = params.get('hidden_dim', 128)
        self.encoding_dim   = params.get('encoding_dim', 1)
        self.n_layers       = params.get('n_layers', 3)

        self.modelname = params["name"] + "_" + datetime.now().strftime("%m%d_%H%M")

        self.params = params

        self.init_model()

    def init_model(self):
        self.encoder = Subnet(self.dim_x, self.encoding_dim*2, self.n_layers, self.hidden_dim)
        self.decoder = Subnet(self.encoding_dim, self.dim_x, self.n_layers, self.hidden_dim)

    def __call__(self, x, parameters=None, rev=False):
        if parameters is None:
            parameters = self.var()

        enc = self.encoder(x, parameters[0])
        mu, sigma = enc[:,:self.encoding_dim], enc[:,self.encoding_dim:]



        if rev: # Use rev as an indicator of whether we want latent or output space
            eps = torch.randn(len(x), self.encoding_dim)
            out = self.decoder(mu + sigma*eps, parameters[1])
            return out
        else:
            return mu

    def jac_det(self, x, parameters=None, rev=False, device=None):
        raise(RuntimeError("Cannot calculate jacobian of regular NN"))

    def fidelity(self, x, y, parameters=None, rev=False, device=None):
        return torch.zeros(len(x))

    def update(self, var):
        self.encoder.update(var[0])
        self.decoder.update(var[1])

    def var(self):
        return [self.encoder.var(), self.decoder.var()]

    def save(self):
        os.makedirs("outputs", exist_ok=True)
        os.makedirs(os.path.join("outputs", self.modelname), exist_ok=True)
        torch.save(self.state_dict(), os.path.join("outputs", self.modelname, "model.pth"))

    def load(self, path):
        state_dict = torch.load(path)
        self.load_state_dict(state_dict)

    def create_samples(self, n_samples=1):
        noise = torch.randn(n_samples, self.encoding_dim) # use gaussian data as input
        samples = self.decoder(noise, self.var()[1])
        # If samples are out of range [0, pi] on target side, wrap them around
        return periodic_shift(samples)
