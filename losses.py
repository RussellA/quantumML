import torch
from util import block_diag_to_full, TORCH_DEVICE
from pennylane.utils import _flatten

#------------------------------------------------------------------------------
# VQC

class CostMSE:
    """Two-Class Mean Squared Error loss for a classifier.
    The labels are expected to be -1, 1.
    Arguments:
    - v: dummy model parameters (unused)
    - lam: influence of the loss function w.r.t. others (unused)"""
    def __init__(self, v, lam=1.0):
        self.lam = lam # UNUSED!

    def __call__(model, X, Y, validation=False):
        # MSE loss for classification
        out = model(X)
        if not validation:
            return MSE_loss(Y, out)
        else:
            return torch.mean(MSE_loss(Y, out))


    def metric_tensor(self, model, X, Y):
        """Computes the metric tensor of the model parameters that are involved
        in the loss computation.
        Arguments:
        - model: The model to compute the metric tensor for
        - X: Batch of input data
        - Y: Batch of input labels (unused)"""
        return torch.mean(model.metric_tensor(X), dim=0)

def MSE_loss(y, out):
    return torch.mean((y - out)**2)


#------------------------------------------------------------------------------
# QINN


class CostGaussian:
    """Gaussian latent loss for a QINN.
    If target groups are turned on, gradients are only computed for the
    forward model parameters.
    Arguments:
    - v: dummy model parameters (unused)
    - lam: influence of the loss function w.r.t. others"""
    def __init__(self, v, lam=1.0):
        self.lam = lam
        self.target_groups = [0, 1]

    def __call__(self, model, X, Y, validation=False):
        # Failsave, if last batch in dataloader has only one element
        if X.ndim == 1:
            X = X.view(1, *X.shape)
        if Y.ndim == 0:
            Y = Y.view(1, *Y.shape)

        out = model(X)
        jacobian = model.jac_det(X)
        return self.lam * gaussian_loss(out, jacobian)


    def metric_tensor(self, model, X, Y):
        """Computes the metric tensor of the model parameters that are involved
        in the loss computation.
        Arguments:
        - model: The model to compute the metric tensor for
        - X: Batch of input data
        - Y: Batch of input labels (unused)"""
        return torch.mean(model.metric_tensor(X), dim=0)


def gaussian_loss(out, jacobian):
    # Gaussian latent loss
    return torch.mean(out**2)/2 - torch.mean(torch.log(torch.abs(jacobian)))/out.shape[1]

#------------------------------------------------------------------------------
# Fidelity


class CostFidelity:
    """Fidelity loss for a QINN.
    If target groups are turned on, all parameters will still receive a gradient
    Arguments:
    - v: dummy model parameters used for determining target groups
    - lam: influence of the loss function w.r.t. others"""
    def __init__(self, v, lam=1.0):
        self.lam = lam
        self.target_groups = list(range(len(v)))


    def __call__(self, model, X, Y, validation=False):
        # Failsave, if last batch in dataloader has only one element
        if X.ndim == 1:
            X = X.view(1, *X.shape)
        if Y.ndim == 0:
            Y = Y.view(1, *Y.shape)
        # Pennylane bugfix
        X.requires_grad = True
        out = model(X)

        fidelity = model.fidelity(X, out)

        if not validation:
            return self.lam*fidelity_loss(fidelity)
        else:
            return self.lam*fidelity_loss(fidelity), torch.mean(fidelity)

    def metric_tensor(self, model, X, Y):
        """Computes the metric tensor of the model parameters that are involved
        in the loss computation.
        Arguments:
        - model: The model to compute the metric tensor for
        - X: Batch of input data
        - Y: Batch of input labels (unused)"""
        out = model(X).detach()
        return torch.mean(model.metric_tensor_fid(out), dim=0)



def fidelity_loss(fidelity, eps=1e-6):
    return - torch.mean(torch.log(fidelity + eps))


class CostIdentity:
    """MSE Identity cost for pretraining the ISP
    Arguments:
    - v: dummy model parameters used for determining target groups
    - lam: influence of the loss function w.r.t. others"""
    def __init__(self, v, lam=1.0, only_isp=False, observable=None, log=False):
        self.lam = lam # UNUSED!
        self.only_isp = only_isp
        self.observable = observable
        self.log = log

    def __call__(self, model, X, Y, validation=False):
        # MSE loss for classification
        if self.only_isp:
            out = model.run_isp(X)
        else:
            latent = model(X)
            out = model(latent, rev=True)
        if not self.observable is None:
            X, out = self.observable(X), self.observable(out)
        if self.log:
            X_norm = X/torch.sum(X, dim=1, keepdims=True)
            out_norm = out/torch.sum(out, dim=1, keepdims=True)

            loss = - torch.mean(torch.log(torch.sum(X_norm * out_norm, dim=1) + 1.e-8))
        else:
            loss = MSE_loss(X, out)

        if validation:
            return self.lam*loss, 1 - torch.mean(torch.abs(X - out))/torch.mean(X)
        else:
            return self.lam*loss



    def metric_tensor(self, model, X, Y):
        """Computes the metric tensor of the model parameters that are involved
        in the loss computation.
        Arguments:
        - model: The model to compute the metric tensor for
        - X: Batch of input data
        - Y: Batch of input labels (unused)"""
        return torch.mean(model.metric_tensor(X), dim=0)


#------------------------------------------------------------------------------
# AutoEncoder

class CostVAE:
    """Reconstruction + variational latent loss for a VAE.
    Arguments:
    - v: dummy model parameters (unused)
    - lam: influence of the latent variational loss"""
    def __init__(self, v, lam=1.0):
        self.lam = lam

    def __call__(self, model, X, Y, validation=False):
        # Failsave, if last batch in dataloader has only one element
        if X.ndim == 1:
            X = X.view(1, *X.shape)
        if Y.ndim == 0:
            Y = Y.view(1, *Y.shape)
        # Pennylane bugfix
        X.requires_grad = True
        noise = torch.randn(X.shape[0], model.encoding_dim, device=TORCH_DEVICE)

        X_rec = model(X, rev=True)
        noise_fake = model(X)

        return MSE_loss(X, X_rec) + self.lam * KLdivergence(noise, noise_fake)

    def metric_tensor(self, model, X, Y):
        raise(NotImplementedError())

#------------------------------------------------------------------------------
# MMD


class CostKL:
    """KL divergence loss for a QINN.
    If target groups are turned on, gradients are only computed for the
    forward model parameters.
    Arguments:
    - v: dummy model parameters used for determining target groups
    - lam: influence of the loss function w.r.t. others
    - kernel_widths: width of the gaussian kernel to use. If this is a list
                     of floats, the sum of multiple kernels will be computed
    - side: The side or the model to compute the KL loss on"""
    def __init__(self, v, lam=1.0, kernel_widths=1.0, side="latent", observable=None, kernel=None):
        self.lam = lam
        self.sigma = kernel_widths
        self.rev = False if side == "latent" else True
        if not self.rev and not observable is None:
            print("Warning, calculating observable for MMD on latent side. Is this really what you want?")
        self.observable = observable
        self.target_groups = [0, 1]
        if kernel is None:
            self.kernel = gaussian_kernel
        else:
            self.kernel = eval(kernel)

    def __call__(self, model, X, Y, validation=False):
        # Failsave, if last batch in dataloader has only one element
        if X.ndim == 1:
            X = X.view(1, *X.shape)
        if Y.ndim == 0:
            Y = Y.view(1, *Y.shape)
        # Pennylane bugfix
        X.requires_grad = True
        noise = torch.randn(X.shape[0], X.shape[1], device=TORCH_DEVICE)

        if self.rev:
            X_fake = model(noise, rev=True)
            self.last_inp = noise
            real = X
            fake = X_fake
        else:
            noise_fake = model(X)
            self.last_inp = X
            real = noise
            fake = noise_fake
        if not self.observable is None:
            real, fake = self.observable(real), self.observable(fake)
        return self.lam * KLdivergence(real, fake, self.kernel, sigma=self.sigma)

    def metric_tensor(self, model, X, Y):
        """Computes the metric tensor of the model parameters that are involved
        in the loss computation.
        Arguments:
        - model: The model to compute the metric tensor for
        - X: Batch of input data
        - Y: Batch of input labels (unused)"""
        return torch.mean(model.metric_tensor(self.last_inp, rev=self.rev), dim=0)



def KLdivergence(X, Y, kernel, sigma=1.0):
    # Case 1: A single kernel width -> calculate one kernel and return
    if type(sigma) in [int, float, torch.float, torch.int]:
        return kernel(X, X, sigma) + kernel(Y, Y, sigma) - 2. * kernel(X, Y, sigma)
    # Case 2: Multiple kernel widths -> calculate each loss independently and
    # add them up together
    elif type(sigma) in [torch.Tensor, list, tuple]:
        div = 0.0
        for sig in sigma:
            div += kernel(X, X, sig) + kernel(Y, Y, sig) - 2. * kernel(X, Y, sig)
        return div
    else:
        raise(ValueError(f"Invalid kernel width {sigma}"))


def gaussian_kernel(X, Y, sigma):
    # Calculate the gaussian Kernel of both vectors, i.e.
    # exp(- || X - Y ||² /(2 * simga²))
    # Calculates the distance of every element in batch X to every element
    # in batch Y, i.e. shapes (b, None, d) - (b, d) -> (b, b, d)
    mat = X[:, None] - Y
    mat = mat ** 2
    if mat.ndim > 2:
        mat = torch.mean(mat, dim=-1)
    mat = -mat/((float(sigma)**2) * 2)
    mat = torch.exp(mat)
    return torch.mean(mat)

def bw_kernel(X, Y, sigma):
    mat = X[:,None]-Y
    mat = mat**2/float(sigma**2)
    if mat.ndim > 2:
        mat = torch.mean(mat, dim=-1)
    mat = mat + 1
    mat = 1./mat
    return torch.mean(mat)
